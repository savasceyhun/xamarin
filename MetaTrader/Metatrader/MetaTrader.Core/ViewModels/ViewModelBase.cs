﻿using MvvmCross.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using MetaTrader.Core.Services;
using MvvmCross.Platform;

namespace MetaTrader.Core.ViewModels
{
    public abstract class ViewModelBase : MvxViewModel
    {       
		private IAppService mAppService;
		public IAppService AppService
		{
			get
			{
				mAppService = mAppService ?? Mvx.Resolve<IAppService>();
				return mAppService;
			}
		}

		private MetaTraderService mMetaTraderService;
		public MetaTraderService MetaTraderService
		{
			get
			{
				mMetaTraderService = mMetaTraderService ?? Mvx.Resolve<MetaTraderService>();
				return mMetaTraderService;
			}
		}

		private MvxCommand mBackCommand;

		public virtual System.Windows.Input.ICommand BackCommand
		{
			get
			{
				mBackCommand = mBackCommand ?? new MvxCommand(()=>
					{		
						this.AppService.CloseView();
					});
				return mBackCommand;
			}
		}

        public virtual void OnFinish()
        {            
        }
    }
}
