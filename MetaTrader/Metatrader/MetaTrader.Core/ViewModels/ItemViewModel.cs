﻿using MvvmCross.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetaTrader.Core.ViewModels
{
    public class ItemViewModel<T>
    {
        public T Model { get; set; }

		private MvxCommand mItemClickCommand;
		public MvxCommand ItemClickCommand
        {
            get
            {
                mItemClickCommand = mItemClickCommand ?? new MvxCommand(() => 
                {
                    if(ItemClickAction != null)
                    {
                        ItemClickAction.Invoke(this.Model);
                    }
                });
				return mItemClickCommand;
            }
        }

        public Action<T> ItemClickAction { get; set; }

        public ItemViewModel(T model)
        {
            Model = model;
        }
    }
}
