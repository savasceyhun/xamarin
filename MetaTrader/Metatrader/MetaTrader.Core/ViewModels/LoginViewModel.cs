﻿using MetaTrader.Core.Models;
using MetaTrader.Core.Services;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetaTrader.Core.ViewModels
{
    public class LoginViewModel : ViewModelBase
    {
        private string mLoginId;
        public string LoginId
        {
            get
            {
                return mLoginId;
            }
            set
            {
                mLoginId = value;
                RaisePropertyChanged(() => LoginId);
            }
        }

        private string mCode;
        public string Code
        {
            get
            {
                return mCode;
            }
            set
            {
                mCode = value;
                RaisePropertyChanged(() => Code);
            }
        }

        private string mPassword;
        public string Password
        {
            get
            {
                return mPassword;
            }
            set
            {
                mPassword = value;
                RaisePropertyChanged(() => Password);
            }
        }

        private MvxCommand mLogOnCommand;

        public System.Windows.Input.ICommand LogOnCommand
        {
            get
            {
                mLogOnCommand = mLogOnCommand ?? new MvxCommand(()=>
                {
                    AppService.ShowProgressDialog("Login...");

                    Task.Run(() =>
                    {
                        var loginResponse = MetaTraderService.Login(LoginId, Password, Code);
                        return loginResponse;
                    }).ContinueWith((t) =>
                    {
                        var loginResponse = t.Result;
                        if (loginResponse.IsSuccess)
                        {
                            this.ShowViewModel<HomeViewModel>();
                        }
                        else
                        {
							AppService.ShowText(loginResponse.MSG);
                        }
							AppService.DismissProgressDialog();
                    }, TaskScheduler.FromCurrentSynchronizationContext());              
                });
                return mLogOnCommand;
            }
        }
			

        public LoginViewModel()
        {

			LoginId = "12208";
			Password = "159951";
			Code = "159951";

   //         LoginId = "5238";
   //         Password = "14072016";
			//Code = "159951";

        }

    }
}
