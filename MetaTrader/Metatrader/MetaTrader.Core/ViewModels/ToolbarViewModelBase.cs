﻿using MvvmCross.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using MetaTrader.Core.Services;
using MvvmCross.Platform;
using System.Dynamic;
using System.Collections;
using Newtonsoft.Json;
using MvvmCross.Core.Platform;

namespace MetaTrader.Core.ViewModels
{
	public abstract class ToolbarViewModelBase : ViewModelBase
    {
		private bool mIsVIOP;
		public bool IsVIOP
		{
			get
			{
				return mIsVIOP;
			}
			set
			{
				mIsVIOP = value;
			}
		}

		public void Init(string isVIOP)
		{
			try
			{
				IsVIOP = bool.Parse(isVIOP);
			}
			catch { }
		}

		protected bool ShowViewModel<TViewModel>(object parameterValuesObject = null,
												 IMvxBundle presentationBundle = null,
												 MvxRequestedBy requestedBy = null)
			where TViewModel : IMvxViewModel
		{
			if (parameterValuesObject == null)
			{
				parameterValuesObject = new
				{
					isVIOP = IsVIOP.ToString()
				};
			}
			else 
			{
				var parameters = parameterValuesObject.ToSimplePropertyDictionary();
				parameters.Add("isVIOP", IsVIOP.ToString());
				parameterValuesObject = parameters;
			}
			return base.ShowViewModel<TViewModel>(parameterValuesObject,
								presentationBundle,
							   	requestedBy);
		}


		private bool mIsShowedEmirlerToolbar = false;
		public bool IsShowedEmirlerToolbar
		{
			get
			{
				return mIsShowedEmirlerToolbar;
			}
			set
			{
				mIsShowedEmirlerToolbar = value;
				RaisePropertyChanged(() => IsShowedEmirlerToolbar);
			}
		}

		private MvxCommand mFiyatlarCommand;
		public virtual MvxCommand FiyatlarCommand
		{
			get
			{
				mFiyatlarCommand = mFiyatlarCommand ?? new MvxCommand(() =>
				 {
					 OnFiyatlarCommand();
				 });
				return mFiyatlarCommand;
			}
		}
		public virtual void OnFiyatlarCommand()
		{
			if (IsVIOP)
			{
				if (!(this is VIOPsViewModel))
				{
					AppService.CloseViewToViewModel(typeof(HomeViewModel), () =>
					{
						ShowViewModel<VIOPsViewModel>();
					});
				}
			}
			else
			{
				if (!(this is StocksViewModel))
				{
					AppService.CloseViewToViewModel(typeof(HomeViewModel), () =>
					{
						ShowViewModel<StocksViewModel>();
					});
				}
			}
		}

		private MvxCommand mHesapOzetiCommand;
		public virtual MvxCommand HesapOzetiCommand
		{
			get
			{
				mHesapOzetiCommand = mHesapOzetiCommand ?? new MvxCommand(() =>
				 {
					 OnHesapOzetiCommand();
				 });
				return mHesapOzetiCommand;
			}
		}
		public virtual void OnHesapOzetiCommand()
		{
			if (!(this is HesapOzetisViewModel))
			{
				AppService.CloseViewToViewModel(typeof(HomeViewModel), () =>
				{
					ShowViewModel<HesapOzetisViewModel>();
				});
			}
		}

		private MvxCommand mEmirlerCommand;
		public virtual MvxCommand EmirlerCommand
		{
			get
			{
				mEmirlerCommand = mEmirlerCommand ?? new MvxCommand(() =>
				 {
					 OnEmirlerCommand();
				 });
				return mEmirlerCommand;
			}
		}
		public virtual void OnEmirlerCommand()
		{
			if (!(this is BekleyensViewModel))
			{
				AppService.CloseViewToViewModel(typeof(HomeViewModel), () =>
				{
					ShowViewModel<BekleyensViewModel>();
				});
			}
		}

		private MvxCommand mPozisyonlarCommand;
		public MvxCommand PozisyonlarCommand
		{
			get
			{
				mPozisyonlarCommand = mPozisyonlarCommand ?? new MvxCommand(() =>
				{
					OnPozisyonlarCommand();
				});
				return mPozisyonlarCommand;
			}
		}
		public virtual void OnPozisyonlarCommand()
		{
			if (!(this is PozisyonlarsViewModel))
			{
				AppService.CloseViewToViewModel(typeof(HomeViewModel), () =>
				{
					ShowViewModel<PozisyonlarsViewModel>();
				});
			}
		}

		private MvxCommand mBekleyenCommand;
		public MvxCommand BekleyenCommand
		{
			get
			{
				mBekleyenCommand = mBekleyenCommand ?? new MvxCommand(() =>
				{
					OnBekleyenCommand();
				});
				return mBekleyenCommand;
			}
		}
		public virtual void OnBekleyenCommand()
		{
			if (!(this is BekleyensViewModel))
			{
				AppService.CloseViewToViewModel(typeof(HomeViewModel), () =>
				{
					ShowViewModel<BekleyensViewModel>();
				});
			}
		}

		private MvxCommand mIptallerCommand;
		public MvxCommand IptallerCommand
		{
			get
			{
				mIptallerCommand = mIptallerCommand ?? new MvxCommand(() =>
				{
					OnIptallerCommand();
				});
				return mIptallerCommand;
			}
		}
		public virtual void OnIptallerCommand()
		{
			if (!(this is IptalsViewModel))
			{
				AppService.CloseViewToViewModel(typeof(HomeViewModel), () =>
				{
					ShowViewModel<IptalsViewModel>();
				});
			}
		}

		private MvxCommand mGerceklesenlerCommand;
		public MvxCommand GerceklesenlerCommand
		{
			get
			{
				mGerceklesenlerCommand = mGerceklesenlerCommand ?? new MvxCommand(() =>
				{
					OnGerceklesenlerCommand();
				});
				return mGerceklesenlerCommand;
			}
		}
		public virtual void OnGerceklesenlerCommand()
		{
			if (!(this is GerceklesenlersViewModel))
			{
				AppService.CloseViewToViewModel(typeof(HomeViewModel), () =>
				{
					ShowViewModel<GerceklesenlersViewModel>();
				});
			}
		}
    }
}
