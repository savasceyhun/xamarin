﻿using MetaTrader.Core.Models;
using MetaTrader.Core.Services;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;
using Newtonsoft.Json;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetaTrader.Core.ViewModels
{
	public class VIOPConfirmOrderViewModel : ToolbarViewModelBase
    {
        private MvxCommand mPlaceOrderCommand;
        public System.Windows.Input.ICommand PlaceOrderCommand
        {
            get
            {
                mPlaceOrderCommand = mPlaceOrderCommand ?? new MvxCommand(() =>
                {
                    AppService.ShowProgressDialog("Trade VIOP...");
                    TradeResponse tradeResponse = new TradeResponse();
                    Task.Run(() =>
                    {                       
						tradeResponse = MetaTraderService.TradeVIOP(Type == "Buy", VIOP.Code,
						                                            Amount, Price, FiyatTipi, EmirTipi, Sure, Tarih);
                    }).ContinueWith((t) =>
                    {
                        AppService.DismissProgressDialog();

                        if (tradeResponse.IsSuccess)
                        {
                            AppService.ShowText("Emriniz basariyla iletilmistir");
                            AppService.CloseView(2);
                        }
                        else
                        {
                            AppService.ShowText("Your Order fail sent" + Environment.NewLine + tradeResponse.MSG);
                            AppService.CloseView();
                        }

                    }, TaskScheduler.FromCurrentSynchronizationContext());
                });


                return mPlaceOrderCommand;
            }
        }

        private MvxCommand mCancelCommand;
        public System.Windows.Input.ICommand CancelCommand
        {
            get
            {
                mCancelCommand = mCancelCommand ?? new MvxCommand(() =>
                {
                    AppService.CloseView();
                });
                return mCancelCommand;
            }
        }

        public void LoadPrice()
        {
			var viopResponse = MetaTraderService.GetVIOP(VIOP.Code);
            if (viopResponse.IsSuccess)
            {
				if(!viopResponse.VIOP.TabanFiyat.Equals(VIOP.TabanFiyat))
                {
                    VIOP.TabanFiyat = viopResponse.VIOP.TabanFiyat;
                    this.InvokeOnMainThread(() =>
                    {
                        AppService.ShowText(string.Format("the base price of {0} stock changed to {1}", VIOP.Name, Math.Round(VIOP.TabanFiyat, 3)));
                    });
                    
                }                
            }
        }


		public int Amount { get; set; }
		public double Price { get; set; }
		public string Type { get; set; }
		public VIOP VIOP { get; set; }

		public FiyatTipi FiyatTipi { get; set; }
		public EmirTipi EmirTipi { get; set; }
		public Sure Sure { get; set; }
		public string Tarih { get; set; }

		public VIOPConfirmOrderViewModel()
        {
			IsVIOP = true;
        }

        public void Init(string viop, double price, int amount, string type, 
		                 string fiyatTipi, string emirTipi, string sure, string tarih)
        {
            VIOP = JsonConvert.DeserializeObject<VIOP>(viop);            
			Price = price;
			Amount = amount;
			Type = type;
            FiyatTipi = (FiyatTipi)Enum.Parse(typeof(FiyatTipi), fiyatTipi);
            EmirTipi = (EmirTipi)Enum.Parse(typeof(EmirTipi), emirTipi);
			Sure = (Sure)Enum.Parse(typeof(Sure), sure);
			Tarih = tarih;
		}
          
    }
}
