﻿using MetaTrader.Core.Models;
using MetaTrader.Core.Services;
using MvvmCross.Core.ViewModels;
using Newtonsoft.Json;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetaTrader.Core.ViewModels
{
    public class VIOPTradeViewModel : ToolbarViewModelBase
    {
		private bool mIsShowedChart = true;
		public bool IsShowedChart
		{
			get
			{
				return mIsShowedChart;
			}
			set
			{
				mIsShowedChart = value;
				mIsShowedSpecs = !mIsShowedSpecs;
				RaisePropertyChanged(() => IsShowedChart);
				RaisePropertyChanged(() => IsShowedSpecs);
			}
		}

		private bool mIsShowedSpecs = false;
		public bool IsShowedSpecs
		{
			get
			{
				return mIsShowedSpecs;
			}
			set
			{
				mIsShowedSpecs = value;
				mIsShowedChart = !mIsShowedChart;
				RaisePropertyChanged(() => IsShowedChart);
				RaisePropertyChanged(() => IsShowedSpecs);
			}
		}

        private int mAmount;
        public int Amount
        {
            get
            {
                return mAmount;
            }
            set
            {
                mAmount = value;
                RaisePropertyChanged(() => Amount);
            }
        }

        private double mPrice;
        public double Price
        {
            get
            {
                return mPrice;
            }
            set
            {
                mPrice = value;
                RaisePropertyChanged(() => Price);
            }
        }

		private string mTarih = DateTime.Now.ToString("dd/MM/yyyy");
		public string Tarih
		{
			get
			{
				if (!TarihEnabled) 
				{
					return string.Empty;
				}

				return mTarih;
			}
			set
			{
				mTarih = value;
				RaisePropertyChanged(() => Tarih);
			}
		}

        private string mTickValue = string.Empty;
        public string TickValue
        {
            get
            {
                if (PriceEnabled)
                {
                    return mTickValue;
                }
                else
                {
                    return string.Empty;
                }
                
            }
            set
            {
                mTickValue = value;
                RaisePropertyChanged(() => TickValue);
            }
        }

        public List<string> mTickValues = new List<string>() {string.Empty};
        public List<string> TickValues
        {
            get
            {
                return mTickValues;
            }
            set
            {
                mTickValues = value;
                RaisePropertyChanged(() => TickValues);
            }
        }

		private VIOP mVIOP;
        public VIOP VIOP
        {
            get
            {
                return mVIOP;
            }
            set
            {
                mVIOP = value;
                RaisePropertyChanged(() => VIOP);
				RaisePropertyChanged(() => BuyMessage);
				RaisePropertyChanged(() => SellMessage);
            }
        }

		public string BuyMessage
		{
			get
			{
				return (this.VIOP != null ? this.VIOP.TabanFiyat.ToString() : string.Empty) + Environment.NewLine + "Buy";
			}
		}

		public string SellMessage
		{
			get
			{
				return (this.VIOP != null ? this.VIOP.TabanFiyat.ToString() : string.Empty) + Environment.NewLine + "Sell";			
			}
		}


        private PlotModel mPlotModel;
        public PlotModel PlotModel
        {
            get
            {
                return mPlotModel;
            }
            set
            {
                mPlotModel = value;
                RaisePropertyChanged(() => PlotModel);
                this.PlotModel.InvalidatePlot(true);
            }
        }
        
        public bool LimitliChecked
        {
            get
            {
				return FiyatTipi == FiyatTipi.Limitli;
            }
            set
            {
				if (value)
				{
					this.FiyatTipi = FiyatTipi.Limitli;
					RaiseOrder();
				}
            }
        }
		public bool EnIyiFiyatChecked
		{
			get
			{
				return FiyatTipi == FiyatTipi.EnIyiFiyat;
			}
			set
			{
				if (value)
				{
					this.FiyatTipi = FiyatTipi.EnIyiFiyat;
					RaiseOrder();
				}
			}
		}
		public bool PiyasaChecked
		{
			get
			{
				return FiyatTipi == FiyatTipi.Piyasa;
			}
			set
			{
				if (value)
				{
					this.FiyatTipi = FiyatTipi.Piyasa;
					RaiseOrder();
				}
			}
		}

		public bool GerceklesmezseIptalEtChecked
		{
			get
			{
				return EmirTipi == EmirTipi.GerceklesmezseIptalEt;
			}
			set
			{
				if (value)
				{
					this.EmirTipi = EmirTipi.GerceklesmezseIptalEt;
					RaiseOrder();
				}
			}
		}
		public bool KalaniIptalEtChecked
		{
			get
			{
				return EmirTipi == EmirTipi.KalaniIptalEt;
			}
			set
			{
				if (value)
				{
					this.EmirTipi = EmirTipi.KalaniIptalEt;
					RaiseOrder();
				}
			}
		}
		public bool KalaniPasifeYazChecked
		{
			get
			{
				return EmirTipi == EmirTipi.KalaniPasifeYaz;
			}
			set
			{
				if (value)
				{
					this.EmirTipi = EmirTipi.KalaniPasifeYaz;
					RaiseOrder();
				}
			}
		}

		public bool GunlukChecked
		{
			get
			{
				return Sure == Sure.Gunluk;
			}
			set
			{
				if (value)
				{
					this.Sure = Sure.Gunluk;
					RaiseOrder();
				}
			}
		}
		public bool IptaleKadarGecerliChecked
		{
			get
			{
				return Sure == Sure.IptaleKadarGecerli;
			}
			set
			{
				if (value)
				{
					this.Sure = Sure.IptaleKadarGecerli;
					RaiseOrder();
				}
			}
		}
		public bool TarihliChecked
		{
			get
			{
				return Sure == Sure.Tarihli;
			}
			set
			{
				if (value)
				{
					this.Sure = Sure.Tarihli;
					RaiseOrder();
				}
			}
		}

		public bool PriceEnabled
		{
			get
			{
				return !(FiyatTipi == FiyatTipi.Piyasa || FiyatTipi == FiyatTipi.EnIyiFiyat);
			}
		}

        public bool TarihEnabled
        {
            get
            {
				return Sure == Sure.Tarihli;
            }
        }

		public FiyatTipi FiyatTipi { get; set; }
		public EmirTipi EmirTipi { get; set; }
		public Sure Sure { get; set; }


		private MvxCommand mSellCommand;
        public System.Windows.Input.ICommand SellCommand
        {
            get
            {
                mSellCommand = mSellCommand ?? new MvxCommand(() =>
                {
					if (Amount > 0)
					{
						this.ShowViewModel<VIOPConfirmOrderViewModel>(new
						{
							viop = JsonConvert.SerializeObject(mVIOP),
							price = TickValue,
							amount = Amount,
							type = "Sell",
							fiyatTipi = FiyatTipi.ToString(),
							emirTipi = EmirTipi.ToString(),
							sure = Sure.ToString(),
							tarih = Tarih,
						});
					}
					else
					{
						AppService.ShowText("Please type Amount > 0");
					}
                });
                return mSellCommand;
            }
        }

        private MvxCommand mBuyCommand;
        public System.Windows.Input.ICommand BuyCommand
        {
            get
            {
                mBuyCommand = mBuyCommand ?? new MvxCommand(() =>
                {
					if (Amount > 0)
					{
						this.ShowViewModel<VIOPConfirmOrderViewModel>(new
						{
							viop = JsonConvert.SerializeObject(mVIOP),
							price = TickValue,
							amount = Amount,
							type = "Buy",
							fiyatTipi = FiyatTipi.ToString(),
							emirTipi = EmirTipi.ToString(),
							sure = Sure.ToString(),
							tarih = Tarih,
						});
					}
					else
					{
						AppService.ShowText("Please type Amount > 0");
					}
                });
                return mBuyCommand;
            }
        }
			
        public VIOPTradeViewModel()
        {
			IsVIOP = true;
        }

        public void RaiseOrder()
        {
			RaisePropertyChanged(() => PriceEnabled);
			RaisePropertyChanged(() => TickValue);
			RaisePropertyChanged(() => Tarih);
			RaisePropertyChanged(() => TarihEnabled);
			RaisePropertyChanged(() => PiyasaChecked);
			RaisePropertyChanged(() => GunlukChecked);
			RaisePropertyChanged(() => TarihliChecked);
			RaisePropertyChanged(() => LimitliChecked);
			RaisePropertyChanged(() => EnIyiFiyatChecked);
			RaisePropertyChanged(() => KalaniIptalEtChecked);
			RaisePropertyChanged(() => GerceklesmezseIptalEtChecked);
			RaisePropertyChanged(() => KalaniPasifeYazChecked);
			RaisePropertyChanged(() => TarihliChecked);


		}

        public void LoadStockWithoutProgressDialog()
        {
			var viopResponse = MetaTraderService.GetVIOP(this.VIOP.Code);
            if (viopResponse.IsSuccess)
            {
				double basePrice = viopResponse.VIOP.TabanFiyat;
				double ceilingPrice = viopResponse.VIOP.TavanFiyat;
				double tickValue = viopResponse.VIOP.TickSize;

				mVIOP = viopResponse.VIOP;

                //mStock.CeilingPrice = stocksResponse.SYU;
                //mStock.BasePrice = stocksResponse.SYL;
                //mStock.TickValue = stocksResponse.SYK;
                //mStock.LastPrice = stocksResponse.SYP;
                //mStock.Ask = stocksResponse.SYA;
                //mStock.Bid = stocksResponse.SYS;
                //mStock.BIG = stocksResponse.BIG;
                //mStock.BRT = stocksResponse.BRT;
                //mStock.TickValue = stocksResponse.SYK;
                //mStock.OperationDate = stocksResponse.SYT;
                //mStock.ValorDate = stocksResponse.SYV;

                mVIOP.Date = DateTime.Now;
                var tickValues = new List<string>();                
				for (double i = mVIOP.TabanFiyat; i <= mVIOP.TavanFiyat; i += mVIOP.TickSize)
                {
                    tickValues.Add(Math.Round(i, 3).ToString());
                }
				if (!tickValues.Contains(Math.Round(this.VIOP.TavanFiyat, 3).ToString()))
				{
					tickValues.Add(Math.Round(this.VIOP.TavanFiyat, 3).ToString());
				}
                tickValues.Reverse();
                tickValues.Add(string.Empty);

                this.InvokeOnMainThread(() =>
                {
					this.VIOP = mVIOP;
					if (mVIOP != null
					    && (!mVIOP.TabanFiyat.Equals(basePrice) || !mVIOP.TavanFiyat.Equals(ceilingPrice) || !mVIOP.TickSize.Equals(tickValue)))
					{
						this.TickValues = tickValues;
					}
					
                });
            }
        }


        public void LoadData()
        {
            this.InvokeOnMainThread(() =>
            {
                AppService.ShowProgressDialog("Loading VIOP...");
                VIOPResponse viopResponse = new VIOPResponse();
				VIOPRecordsResponse viopRecordsResponse = new VIOPRecordsResponse();
                Task.Run(() =>
                {
					viopResponse = MetaTraderService.GetVIOP(this.VIOP.Code);
					viopRecordsResponse = MetaTraderService.GetVIOPRecords(this.VIOP.Code);
                }).ContinueWith((t) =>
                {
                    if (viopResponse.IsSuccess)
                    {
						//mStock.CeilingPrice = stocksResponse.SYU;
						//mStock.BasePrice = stocksResponse.SYL;
						//mStock.TickValue = stocksResponse.SYK;
						//mStock.LastPrice = stocksResponse.SYP;
						//mStock.Ask = stocksResponse.SYA;
						//mStock.Bid = stocksResponse.SYS;
						//mStock.BIG = stocksResponse.BIG;
						//mStock.BRT = stocksResponse.BRT;
						//mStock.TickValue = stocksResponse.SYK;
						//mStock.OperationDate = stocksResponse.SYT;
						//mStock.ValorDate = stocksResponse.SYV;

						mVIOP = viopResponse.VIOP;
						mVIOP.Date = DateTime.Now;
						this.VIOP = mVIOP;

						var tickValue = Math.Max(this.VIOP.TickSize, 0.01);
                        var tickValues = new List<string>();
						for (double i = this.VIOP.TabanFiyat; i <= this.VIOP.TavanFiyat; i += tickValue)
                        {
                            tickValues.Add(Math.Round(i, 3).ToString());
                        }
						if (!tickValues.Contains(Math.Round(this.VIOP.TavanFiyat, 3).ToString()))
						{
							tickValues.Add(Math.Round(this.VIOP.TavanFiyat, 3).ToString());
						}

						tickValues.Reverse();
                        tickValues.Add(string.Empty);
                        this.TickValues = tickValues;
						this.TickValue = Math.Round(this.VIOP.TabanFiyat, 3).ToString();

					}
                    else
                    {
                        AppService.ShowText(viopResponse.MSG);
                    }

                    this.PlotModel.Series.Clear();
                    if (viopRecordsResponse.IsSuccess)
                    {
                        var bidSeries = new LineSeries
                        {
                            MarkerType = MarkerType.None,
                            Color = OxyColors.Blue
                        };
                        int i = 0;
                        var bidDataPoint = viopRecordsResponse.Prices.Select(p =>
                        {
                            return new DataPoint(i++, p);
                        });
                        bidSeries.Points.AddRange(bidDataPoint);
                        this.PlotModel.Series.Add(bidSeries);
                    }
                    else
                    {
                        AppService.ShowText(viopRecordsResponse.MSG);
                    }

                    RaisePropertyChanged(() => this.PlotModel);
                    this.PlotModel.InvalidatePlot(true);

                    AppService.DismissProgressDialog();
                }, TaskScheduler.FromCurrentSynchronizationContext());
            });
        }

        public void Init(string stock)
        {            
			this.VIOP = JsonConvert.DeserializeObject<VIOP>(stock);

            this.LoadData();



            var plotModel = new PlotModel
            {
                Title = string.Empty,
                TitleFontSize = 0,
                PlotAreaBorderThickness = new OxyThickness(0),
                LegendBorderThickness = 0,
                PlotAreaBorderColor = OxyColors.Transparent,
            };
            plotModel.Axes.Add(new LinearAxis
            {
                Position = AxisPosition.Bottom,
                IsPanEnabled = false,
                IsZoomEnabled = false,
                LabelFormatter = x => string.Empty,
                FontSize = 0,
                TickStyle = TickStyle.None,
                AxislineStyle = LineStyle.Solid,
            });
            plotModel.Axes.Add(new LinearAxis
            {
                Position = AxisPosition.Left,
                IsPanEnabled = false,
                IsZoomEnabled = false,
                TickStyle = TickStyle.None,
                AxislineStyle = LineStyle.Solid,
                MajorGridlineStyle = LineStyle.Solid,
                MinorGridlineStyle = LineStyle.Solid,
            });
            this.PlotModel = plotModel;
        }
    }

	public enum FiyatTipi
	{
		Limitli, Piyasa, EnIyiFiyat
	}

	public enum Sure
	{
		Gunluk, IptaleKadarGecerli, Tarihli
	}

	public enum EmirTipi
	{
		KalaniPasifeYaz, KalaniIptalEt, GerceklesmezseIptalEt
	}
}
