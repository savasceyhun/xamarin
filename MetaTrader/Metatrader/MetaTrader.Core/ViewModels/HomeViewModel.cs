﻿using MvvmCross.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetaTrader.Core.ViewModels
{
    public class HomeViewModel : ViewModelBase
    {
        private MvxCommand mAccountCommand;
        public System.Windows.Input.ICommand AccountCommand
        {
            get
            {
                mAccountCommand = mAccountCommand ?? new MvxCommand(() =>
                {
                    //this.ShowViewModel<LoginViewModel>();
                });
                return mAccountCommand;
            }
        }

        private MvxCommand mStocksCommand;
        public System.Windows.Input.ICommand StocksCommand
        {
            get
            {
                mStocksCommand = mStocksCommand ?? new MvxCommand(() =>
                {
                    this.ShowViewModel<StocksViewModel>();
                });
                return mStocksCommand;
            }
        }

        private MvxCommand mDerivativesCommand;
        public System.Windows.Input.ICommand DerivativesCommand
        {
            get
            {
                mDerivativesCommand = mDerivativesCommand ?? new MvxCommand(() =>
                {
                    this.ShowViewModel<VIOPsViewModel>();
                });
                return mDerivativesCommand;
            }
        }

        private MvxCommand mMoneyTransfersCommand;
        public System.Windows.Input.ICommand MoneyTransfersCommand
        {
            get
            {
                mMoneyTransfersCommand = mMoneyTransfersCommand ?? new MvxCommand(() =>
                {
                    //this.ShowViewModel<LoginViewModel>();
                });
                return mMoneyTransfersCommand;
            }
        }
    }
}
