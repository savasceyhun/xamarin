﻿using MvvmCross.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MetaTrader.Core.Models;

namespace MetaTrader.Core.ViewModels
{
	public class PozisyonlarItemViewModel : ItemViewModel<Pozisyonlar>
	{
		private MvxCommand mSATClickCommand;
		public MvxCommand SATClickCommand
		{
			get
			{
				mSATClickCommand = mSATClickCommand ?? new MvxCommand(() =>
				{
					if (SATClickAction != null)
					{
						SATClickAction.Invoke(this.Model);
					}
				});
				return mSATClickCommand;
			}
		}

		public Action<Pozisyonlar> SATClickAction { get; set; }

		public PozisyonlarItemViewModel(Pozisyonlar pozisyonlar) : base(pozisyonlar)
		{
		}
	}
}
