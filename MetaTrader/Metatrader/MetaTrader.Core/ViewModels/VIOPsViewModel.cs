﻿using MetaTrader.Core.Models;
using MetaTrader.Core.Services;
using MvvmCross.Core.ViewModels;
using Newtonsoft.Json;
using OxyPlot;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetaTrader.Core.ViewModels
{
    public class VIOPsViewModel : ToolbarViewModelBase
    {
        private string mSearchText = string.Empty;
        public string SearchText
        {
            get
            {
                return mSearchText;
            }
            set
            {
                mSearchText = value;
                RaisePropertyChanged(() => SearchText);
                RaisePropertyChanged(() => VIOPsList);
            }
        }

        List<ItemViewModel<VIOP>> mVIOPsList = new List<ItemViewModel<VIOP>>();
        public List<ItemViewModel<VIOP>> VIOPsList
        {
            get
            {
                return mVIOPsList.Where(t=> t.Model.Name.ToUpper().Contains(SearchText.ToUpper())).ToList();
            }
        }

        private Action<VIOP> mStockItemClickAction;
        public Action<VIOP> StockItemClickAction
        {
            get
            {
                mStockItemClickAction = mStockItemClickAction ?? new Action<VIOP>((t) =>
                {
					this.ShowViewModel<VIOPTradeViewModel>(new
                    {
                        stock = JsonConvert.SerializeObject(t),
                    });
                });
                return mStockItemClickAction;
            }
		}

        public VIOPsViewModel()
        {
			IsVIOP = true;
            LoadVIOPs();
        }

        public void LoadVIOPs()
        {
            this.InvokeOnMainThread(() =>
            {
                AppService.ShowProgressDialog("Loading VIOP...");
                Task.Run(() =>
                {
					var stocksResponse = MetaTraderService.GetVIOPs();
                    return stocksResponse;
                }).ContinueWith((t) =>
                {
                    var stocksResponse = t.Result;
                    if (stocksResponse.IsSuccess)
                    {
						mVIOPsList = new List<ItemViewModel<VIOP>>(stocksResponse.VIOPs.Select(s =>
                        {
                            ItemViewModel<VIOP> itemViewModel = new ItemViewModel<VIOP>(s);
                            itemViewModel.ItemClickAction = this.StockItemClickAction;
                            return itemViewModel;
                        }));
                    }
                    else
                    {
                        AppService.ShowText(stocksResponse.MSG);
                    }
					RaisePropertyChanged(() => VIOPsList);
					AppService.DismissProgressDialog();
                }, TaskScheduler.FromCurrentSynchronizationContext());
            });
        }
    }
}
