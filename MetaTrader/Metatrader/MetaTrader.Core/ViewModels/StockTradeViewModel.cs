﻿using MetaTrader.Core.Models;
using MetaTrader.Core.Services;
using MvvmCross.Core.ViewModels;
using Newtonsoft.Json;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetaTrader.Core.ViewModels
{
    public class StockTradeViewModel : ToolbarViewModelBase
    {
		private bool mIsShowedChart = true;
		public bool IsShowedChart
		{
			get
			{
				return mIsShowedChart;
			}
			set
			{
				mIsShowedChart = value;
				mIsShowedSpecs = !mIsShowedSpecs;
				RaisePropertyChanged(() => IsShowedChart);
				RaisePropertyChanged(() => IsShowedSpecs);
			}
		}

		private bool mIsShowedSpecs = false;
		public bool IsShowedSpecs
		{
			get
			{
				return mIsShowedSpecs;
			}
			set
			{
				mIsShowedSpecs = value;
				mIsShowedChart = !mIsShowedChart;
				RaisePropertyChanged(() => IsShowedChart);
				RaisePropertyChanged(() => IsShowedSpecs);
			}
		}

        private int mAmount;
        public int Amount
        {
            get
            {
                return mAmount;
            }
            set
            {
                mAmount = value;
                RaisePropertyChanged(() => Amount);
            }
        }

        private double mPrice;
        public double Price
        {
            get
            {
                return mPrice;
            }
            set
            {
                mPrice = value;
                RaisePropertyChanged(() => Price);
            }
        }

        private string mTickValue = string.Empty;
        public string TickValue
        {
            get
            {
                if (PriceEnabled)
                {
                    return mTickValue;
                }
                else
                {
                    return string.Empty;
                }
                
            }
            set
            {
                mTickValue = value;
                RaisePropertyChanged(() => TickValue);
            }
        }

        public List<string> mTickValues = new List<string>() {string.Empty};
        public List<string> TickValues
        {
            get
            {
                return mTickValues;
            }
            set
            {
                mTickValues = value;
                RaisePropertyChanged(() => TickValues);
            }
        }

        private Stock mStock;
        public Stock Stock
        {
            get
            {
                return mStock;
            }
            set
            {
                mStock = value;
                RaisePropertyChanged(() => Stock);
				RaisePropertyChanged(() => BuyMessage);
				RaisePropertyChanged(() => SellMessage);
            }
        }

		public string BuyMessage
		{
			get
			{
				return (mStock != null ? mStock.LastPrice.ToString() : string.Empty) + Environment.NewLine + "Buy";
			}
		}

		public string SellMessage
		{
			get
			{
				return (mStock != null ? mStock.LastPrice.ToString() : string.Empty) + Environment.NewLine + "Sell";			
			}
		}


        private PlotModel mPlotModel;
        public PlotModel PlotModel
        {
            get
            {
                return mPlotModel;
            }
            set
            {
                mPlotModel = value;
                RaisePropertyChanged(() => PlotModel);
                this.PlotModel.InvalidatePlot(true);
            }
        }
        
        public bool LimitChecked
        {
            get
            {
                return OrderType == OrderType.Limit;
            }
            set
            {
                if(OrderType != OrderType.Limit)
                {
                    OrderType = OrderType.Limit;
                    OrderMethod = OrderMethod.Gunluk;
                    RaiseOrder();
                }
            }
        }

        public bool PiyasaChecked
        {
            get
            {
                return OrderType == OrderType.Piyasa;
            }
            set
            {
                OrderType = OrderType.Piyasa;
                OrderMethod = OrderMethod.KIE;
                RaiseOrder();
            }
        }

        public bool PiyasadanLimiteChecked
        {
            get
            {
                return OrderType == OrderType.PiyasadanLimite;
            }
            set
            {
                if (OrderType != OrderType.PiyasadanLimite)
                {
                    OrderType = OrderType.PiyasadanLimite;
                    OrderMethod = OrderMethod.Gunluk;
                    RaiseOrder();
                }
            }
        }

        public bool DengeleyiciChecked
        {
            get
            {
                return OrderType == OrderType.Dengeleyici;
            }
            set
            {
                OrderType = OrderType.Dengeleyici;
                OrderMethod = OrderMethod.IMB;
                RaiseOrder();
            }
        }

        public bool GunlukChecked
        {
            get
            {
                return OrderMethod == OrderMethod.Gunluk;
            }
            set
            {
                OrderMethod = OrderMethod.Gunluk;
                RaiseOrder();
            }
        }

        public bool KIEChecked
        {
            get
            {
                return (OrderMethod == OrderMethod.KIE) || (OrderType == OrderType.Dengeleyici && OrderMethod == OrderMethod.IMB);
            }
            set
            {
                OrderMethod = OrderMethod.KIE;
                RaiseOrder();
            }
        }

        public bool OrderMethodEnabled
        {
            get
            {
                return OrderType == OrderType.Limit || OrderType == OrderType.PiyasadanLimite;
            }
        }

        public bool PriceEnabled
        {
            get
            {
                return OrderType == OrderType.Limit;
            }
        }

        public OrderType OrderType { get; set; }
        public OrderMethod OrderMethod { get; set; }


        private MvxCommand mSellCommand;
        public System.Windows.Input.ICommand SellCommand
        {
            get
            {
                mSellCommand = mSellCommand ?? new MvxCommand(() =>
                {
					if (Amount > 0)
					{
						this.ShowViewModel<StockConfirmOrderViewModel>(new
						{
							stock = JsonConvert.SerializeObject(mStock),
							price = TickValue,
							amount = Amount,
							type = "Sell",
							ordertype = OrderType.ToString(),
							ordermethod = OrderMethod.ToString(),
						});
					}
					else
					{
						AppService.ShowText("Please type Amount > 0");
					}
                });
                return mSellCommand;
            }
        }

        private MvxCommand mBuyCommand;
        public System.Windows.Input.ICommand BuyCommand
        {
            get
            {
                mBuyCommand = mBuyCommand ?? new MvxCommand(() =>
                {
					if (Amount > 0)
					{
						this.ShowViewModel<StockConfirmOrderViewModel>(new
						{
							stock = JsonConvert.SerializeObject(mStock),
							price = TickValue,
							amount = Amount,
							type = "Buy",
							ordertype = OrderType.ToString(),
							ordermethod = OrderMethod.ToString(),
						});
					}
					else
					{
						AppService.ShowText("Please type Amount > 0");
					}
                });
                return mBuyCommand;
            }
        }
			
        public StockTradeViewModel()
        {
        }

        public void RaiseOrder()
        {
            RaisePropertyChanged(() => DengeleyiciChecked);
            RaisePropertyChanged(() => PiyasadanLimiteChecked);
            RaisePropertyChanged(() => PiyasaChecked);
            RaisePropertyChanged(() => LimitChecked);
            RaisePropertyChanged(() => GunlukChecked);
            RaisePropertyChanged(() => KIEChecked);
            RaisePropertyChanged(() => OrderMethodEnabled);
            RaisePropertyChanged(() => TickValue);
            RaisePropertyChanged(() => PriceEnabled);
        }

        public void LoadStockWithoutProgressDialog()
        {
            var stocksResponse = MetaTraderService.GetStock(this.Stock.Name);
            if (stocksResponse.IsSuccess)
            {
				double basePrice = mStock.BasePrice;
				double ceilingPrice = mStock.CeilingPrice;
				double tickValue = mStock.TickValue;

                mStock.CeilingPrice = stocksResponse.SYU;
                mStock.BasePrice = stocksResponse.SYL;
                mStock.TickValue = stocksResponse.SYK;
                mStock.LastPrice = stocksResponse.SYP;
                mStock.Ask = stocksResponse.SYA;
                mStock.Bid = stocksResponse.SYS;
                mStock.BIG = stocksResponse.BIG;
                mStock.BRT = stocksResponse.BRT;
                mStock.TickValue = stocksResponse.SYK;
                mStock.OperationDate = stocksResponse.SYT;
                mStock.ValorDate = stocksResponse.SYV;

                mStock.Date = DateTime.Now;
                var tickValues = new List<string>();                
                for (double i = mStock.BasePrice; i <= mStock.CeilingPrice; i += mStock.TickValue)
                {
                    tickValues.Add(Math.Round(i, 2).ToString());
                }
				if (!tickValues.Contains(Math.Round(mStock.CeilingPrice, 2).ToString()))
				{
					tickValues.Add(Math.Round(mStock.CeilingPrice, 2).ToString());
				}

                tickValues.Reverse();
                tickValues.Add(string.Empty);

                this.InvokeOnMainThread(() =>
                {
					this.Stock = mStock;
					if (mStock != null
						   && (!mStock.BasePrice.Equals(basePrice) || !mStock.CeilingPrice.Equals(ceilingPrice) || !mStock.TickValue.Equals(tickValue)))
					{
						this.TickValues = tickValues;
					}
					
                });
            }
        }


        public void LoadData()
        {
            this.InvokeOnMainThread(() =>
            {
                AppService.ShowProgressDialog("Loading stock...");
                StockResponse stocksResponse = new StockResponse();
                StockRecordsResponse stockRecordsResponse = new StockRecordsResponse();
                Task.Run(() =>
                {
                    stocksResponse = MetaTraderService.GetStock(this.Stock.Name);
                    stockRecordsResponse = MetaTraderService.GetStockRecords(this.Stock.Name);
                }).ContinueWith((t) =>
                {
                    if (stocksResponse.IsSuccess)
                    {
                        mStock.CeilingPrice = stocksResponse.SYU;
                        mStock.BasePrice = stocksResponse.SYL;
                        mStock.TickValue = stocksResponse.SYK;
                        mStock.LastPrice = stocksResponse.SYP;
                        mStock.Ask = stocksResponse.SYA;
                        mStock.Bid = stocksResponse.SYS;
                        mStock.BIG = stocksResponse.BIG;
                        mStock.BRT = stocksResponse.BRT;
                        mStock.TickValue = stocksResponse.SYK;
                        mStock.OperationDate = stocksResponse.SYT;
                        mStock.ValorDate = stocksResponse.SYV;

						this.Stock = mStock;

                        var tickValue = Math.Max(mStock.TickValue, 0.01);
                        var tickValues = new List<string>();
                        for (double i = mStock.BasePrice; i <= mStock.CeilingPrice; i += tickValue)
                        {
                            tickValues.Add(Math.Round(i, 2).ToString());
                        }

						if (!tickValues.Contains(Math.Round(mStock.CeilingPrice, 2).ToString()))
						{
							tickValues.Add(Math.Round(mStock.CeilingPrice, 2).ToString());
						}

                        tickValues.Reverse();
                        tickValues.Add(string.Empty);                        
                        this.TickValues = tickValues;
						this.TickValue = Math.Round(mStock.LastPrice, 2).ToString();
                    }
                    else
                    {
                        AppService.ShowText(stocksResponse.MSG);
                    }

                    this.PlotModel.Series.Clear();
                    if (stockRecordsResponse.IsSuccess)
                    {
                        var bidSeries = new LineSeries
                        {
                            MarkerType = MarkerType.None,
                            Color = OxyColors.Blue
                        };
                        int i = 0;
                        var bidDataPoint = stockRecordsResponse.Prices.Select(p =>
                        {
                            return new DataPoint(i++, p);
                        });
                        bidSeries.Points.AddRange(bidDataPoint);
                        this.PlotModel.Series.Add(bidSeries);
                    }
                    else
                    {
                        AppService.ShowText(stockRecordsResponse.MSG);
                    }

                    RaisePropertyChanged(() => this.PlotModel);
                    this.PlotModel.InvalidatePlot(true);

                    AppService.DismissProgressDialog();
                }, TaskScheduler.FromCurrentSynchronizationContext());
            });
        }

        public void Init(string stock)
        {            
            this.Stock = JsonConvert.DeserializeObject<Stock>(stock);

            this.OrderType = OrderType.Limit;
            this.OrderMethod = OrderMethod.Gunluk;

            this.LoadData();
            var plotModel = new PlotModel
            {
                Title = string.Empty,
                TitleFontSize = 0,
                PlotAreaBorderThickness = new OxyThickness(0),
                LegendBorderThickness = 0,
                PlotAreaBorderColor = OxyColors.Transparent,
            };
            plotModel.Axes.Add(new LinearAxis
            {
                Position = AxisPosition.Bottom,
                IsPanEnabled = false,
                IsZoomEnabled = false,
                LabelFormatter = x => string.Empty,
                FontSize = 0,
                TickStyle = TickStyle.None,
                AxislineStyle = LineStyle.Solid,
            });
            plotModel.Axes.Add(new LinearAxis
            {
                Position = AxisPosition.Left,
                IsPanEnabled = false,
                IsZoomEnabled = false,
                TickStyle = TickStyle.None,
                AxislineStyle = LineStyle.Solid,
                MajorGridlineStyle = LineStyle.Solid,
                MinorGridlineStyle = LineStyle.Solid,
            });
            this.PlotModel = plotModel;
        }
    }

    public enum OrderType
    {
        Limit, Piyasa, PiyasadanLimite, Dengeleyici
    }

    public enum OrderMethod
    {
        Gunluk, KIE, IMB
    }
}
