﻿using MetaTrader.Core.Models;
using MetaTrader.Core.Services;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;
using Newtonsoft.Json;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetaTrader.Core.ViewModels
{
    public class StockConfirmOrderViewModel : ViewModelBase
    {
        private MvxCommand mPlaceOrderCommand;
        public System.Windows.Input.ICommand PlaceOrderCommand
        {
            get
            {
                mPlaceOrderCommand = mPlaceOrderCommand ?? new MvxCommand(() =>
                {
                    AppService.ShowProgressDialog("Trade stock...");
                    TradeResponse tradeResponse = new TradeResponse();
                    Task.Run(() =>
                    {                       
                        tradeResponse = MetaTraderService.TradeStock(mType == "Buy", Stock.Name,
                            Amount, Price, OrderType, OrderMethod);
                    }).ContinueWith((t) =>
                    {
                        AppService.DismissProgressDialog();

                        if (tradeResponse.IsSuccess)
                        {
                            AppService.ShowText("Your Order successfully sent");
                            AppService.CloseView(2);
                        }
                        else
                        {
                            AppService.ShowText("Your Order fail sent" + Environment.NewLine + tradeResponse.MSG);
                            AppService.CloseView();
                        }

                    }, TaskScheduler.FromCurrentSynchronizationContext());
                });


                return mPlaceOrderCommand;
            }
        }

        private MvxCommand mCancelCommand;
        public System.Windows.Input.ICommand CancelCommand
        {
            get
            {
                mCancelCommand = mCancelCommand ?? new MvxCommand(() =>
                {
					AppService.CloseView();
                });
                return mCancelCommand;
            }
        }

        public string OrderMessage
        {
            get
            {
                return string.Format("{0} {1} {2} @ {3} LMT DAY", mType, Stock.Name, Amount, Price);
            }
        }

        public void LoadPrice()
        {
            var stocksResponse = MetaTraderService.GetStock(Stock.Name);
            if (stocksResponse.IsSuccess)
            {
                if(stocksResponse.SYP != Stock.LastPrice)
                {
                    Stock.CeilingPrice = stocksResponse.SYP;
                    this.InvokeOnMainThread(() =>
                    {
                        AppService.ShowText(string.Format("the price of {0} stock changed to {1}", Stock.Name, Math.Round(Stock.LastPrice, 2)));
                    });
                    
                }                
            }
        }

        private string mType;

        public int Amount { get; set; }
        public double Price { get; set; }
        public string Type { get; set; }
        public Stock Stock { get; set; }
        public OrderType OrderType { get; set; }
        public OrderMethod OrderMethod { get; set; }

        public StockConfirmOrderViewModel()
        {
        }


        public void Init(string stock, double price, int amount, string type, string ordertype, string ordermethod)
        {
            Stock = JsonConvert.DeserializeObject<Stock>(stock);
            Price = price == 0 ? Stock.CeilingPrice : price;
            Amount = amount;
            mType = type;
            OrderType = (OrderType)Enum.Parse(typeof(OrderType), ordertype);
            OrderMethod = (OrderMethod)Enum.Parse(typeof(OrderMethod), ordermethod);
        }
          
    }
}
