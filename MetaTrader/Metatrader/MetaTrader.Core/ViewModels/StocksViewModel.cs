﻿using MetaTrader.Core.Models;
using MetaTrader.Core.Services;
using MvvmCross.Core.ViewModels;
using Newtonsoft.Json;
using OxyPlot;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetaTrader.Core.ViewModels
{
    public class StocksViewModel : ToolbarViewModelBase
    {
        private string mSearchText = string.Empty;
        public string SearchText
        {
            get
            {
                return mSearchText;
            }
            set
            {
                mSearchText = value;
                RaisePropertyChanged(() => SearchText);
                RaisePropertyChanged(() => StocksList);
            }
        }

        List<ItemViewModel<Stock>> mStocks = new List<ItemViewModel<Stock>>();
        public List<ItemViewModel<Stock>> StocksList
        {
            get
            {
                return mStocks.Where(t =>
                {
                    return t.Model.Name.ToUpper().Contains(SearchText.ToUpper());
                }).ToList();
            }
        }

        private Action<Stock> mStockItemClickAction;
        public Action<Stock> StockItemClickAction
        {
            get
            {
                mStockItemClickAction = mStockItemClickAction ?? new Action<Stock>((t) =>
                {
                    this.ShowViewModel<StockTradeViewModel>(new
                    {
                        stock = JsonConvert.SerializeObject(t)
                    });
                });
                return mStockItemClickAction;
            }
        }

        public StocksViewModel()
        {
            LoadStocks();
        }

        public void LoadStocks()
        {
            this.InvokeOnMainThread(() =>
            {
                AppService.ShowProgressDialog("Loading stocks...");
                Task.Run(() =>
                {
                    var stocksResponse = MetaTraderService.GetStocks();
                    return stocksResponse;
                }).ContinueWith((t) =>
                {
                    var stocksResponse = t.Result;
                    if (stocksResponse.IsSuccess)
                    {
                        mStocks = new List<ItemViewModel<Stock>>(stocksResponse.Stocks.Select(s =>
                        {
                            ItemViewModel<Stock> itemViewModel = new ItemViewModel<Stock>(s);
                            itemViewModel.ItemClickAction = this.StockItemClickAction;
                            return itemViewModel;
                        }));
                    }
                    else
                    {
                        AppService.ShowText(stocksResponse.MSG);
                    }
					RaisePropertyChanged(() => StocksList);
					AppService.DismissProgressDialog();
                }, TaskScheduler.FromCurrentSynchronizationContext());
            });
        }
    }
}
