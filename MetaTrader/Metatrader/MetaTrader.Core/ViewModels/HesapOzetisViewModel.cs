﻿using MetaTrader.Core.Models;
using MetaTrader.Core.Services;
using MvvmCross.Core.ViewModels;
using Newtonsoft.Json;
using OxyPlot;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetaTrader.Core.ViewModels
{
    public class HesapOzetisViewModel : ToolbarViewModelBase
    {
        private string mSearchText = string.Empty;
        public string SearchText
        {
            get
            {
                return mSearchText;
            }
            set
            {
                mSearchText = value;
                RaisePropertyChanged(() => SearchText);
                RaisePropertyChanged(() => HesapOzetisList);
            }
        }

		List<ItemViewModel<HesapOzeti>> mHesapOzetis = new List<ItemViewModel<HesapOzeti>>();
		public List<ItemViewModel<HesapOzeti>> HesapOzetisList
        {
            get
            {
                return mHesapOzetis.Where(t =>
                {
                    return true;
                }).ToList();
            }
        }

        private Action<HesapOzeti> mHesapOzetiItemClickAction;
		public Action<HesapOzeti> HesapOzetiItemClickAction
        {
            get
            {
                mHesapOzetiItemClickAction = mHesapOzetiItemClickAction ?? new Action<HesapOzeti>((t) =>
                {
                    //this.ShowViewModel<StockTradeViewModel>(new
                    //{
                    //    stock = JsonConvert.SerializeObject(t)
                    //});
                });
                return mHesapOzetiItemClickAction;
            }
        }

        public HesapOzetisViewModel()
        {
            LoadStocks();
        }

        public void LoadStocks()
        {
            this.InvokeOnMainThread(() =>
            {
                AppService.ShowProgressDialog("Loading hesap ozetis...");
                Task.Run(() =>
                {
					var hesapozetisResponse = MetaTraderService.GetHesapOzetis();
                    return hesapozetisResponse;
                }).ContinueWith((t) =>
                {
					var hesapozetisResponse = t.Result;
                    if (hesapozetisResponse.IsSuccess)
                    {
						mHesapOzetis = new List<ItemViewModel<HesapOzeti>>(hesapozetisResponse.HesapOzetis.Select(s =>
                        {
							var itemViewModel = new ItemViewModel<HesapOzeti>(s);
                            itemViewModel.ItemClickAction = this.HesapOzetiItemClickAction;
							return itemViewModel;
                        }));
                    }
                    else
                    {
                        AppService.ShowText(hesapozetisResponse.MSG);
                    }
					RaisePropertyChanged(() => HesapOzetisList);
					AppService.DismissProgressDialog();
                }, TaskScheduler.FromCurrentSynchronizationContext());
            });
        }
    }
}
