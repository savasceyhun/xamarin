﻿using MetaTrader.Core.Models;
using MetaTrader.Core.Services;
using MvvmCross.Core.ViewModels;
using Newtonsoft.Json;
using OxyPlot;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetaTrader.Core.ViewModels
{
    public class BekleyensViewModel : ToolbarViewModelBase
    {
        private string mSearchText = string.Empty;
        public string SearchText
        {
            get
            {
                return mSearchText;
            }
            set
            {
                mSearchText = value;
                RaisePropertyChanged(() => SearchText);
                RaisePropertyChanged(() => PozisyonlarsList);
            }
        }

		List<PozisyonlarItemViewModel> mPozisyonlars = new List<PozisyonlarItemViewModel>();
		public List<PozisyonlarItemViewModel> PozisyonlarsList
        {
            get
            {
                return mPozisyonlars.Where(t =>
                {
                    return true;
                }).ToList();
            }
        }

        private Action<Pozisyonlar> mStockItemClickAction;
        public Action<Pozisyonlar> StockItemClickAction
        {
            get
            {
                mStockItemClickAction = mStockItemClickAction ?? new Action<Pozisyonlar>((t) =>
                {
                    //this.ShowViewModel<StockTradeViewModel>(new
                    //{
                    //    stock = JsonConvert.SerializeObject(t)
                    //});
                });
                return mStockItemClickAction;
            }
        }

		private Action<Pozisyonlar> mSATClickAction;
		public Action<Pozisyonlar> SATClickAction
		{
			get
			{
				mSATClickAction = mSATClickAction ?? new Action<Pozisyonlar>((t) =>
				{
					this.ShowViewModel<StockTradeViewModel>(new
					{
						stock = JsonConvert.SerializeObject(new Stock()
						{
							Name = t.MenkulAdi
						})
					});
				});
				return mSATClickAction;
			}
		}

        public BekleyensViewModel()
        {
			IsShowedEmirlerToolbar = true;
            //LoadStocks();
        }

        public void LoadStocks()
        {
            this.InvokeOnMainThread(() =>
            {
                AppService.ShowProgressDialog("Loading bekleyens...");
                Task.Run(() =>
                {
					//var stocksResponse = MetaTraderService.GetPozisyonlars();
     //               return stocksResponse;
                }).ContinueWith((t) =>
                {
					//var stocksResponse = t.Result;
     //               if (stocksResponse.IsSuccess)
     //               {
					//	mPozisyonlars = new List<PozisyonlarItemViewModel>(stocksResponse.Pozisyonlars.Select(s =>
     //                   {
					//		PozisyonlarItemViewModel itemViewModel = new PozisyonlarItemViewModel(s);
     //                       itemViewModel.ItemClickAction = this.StockItemClickAction;
					//		itemViewModel.SATClickAction = this.SATClickAction;
					//		return itemViewModel;
     //                   }));
     //               }
     //               else
     //               {
     //                   AppService.ShowText(stocksResponse.MSG);
     //               }
					//RaisePropertyChanged(() => PozisyonlarsList);
					AppService.DismissProgressDialog();
                }, TaskScheduler.FromCurrentSynchronizationContext());
            });
        }
    }
}
