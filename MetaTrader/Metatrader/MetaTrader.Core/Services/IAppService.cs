﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetaTrader.Core.Services
{
    public interface IAppService
    {
        void ShowText(string message);
        void ShowDialog(string message);
        void ShowProgressDialog(string message);
        void DismissProgressDialog();
        void CloseView(int countClose = 1, Action action = null);
		void CloseViewToViewModel(Type viewModelType, Action action = null);
    }
}
