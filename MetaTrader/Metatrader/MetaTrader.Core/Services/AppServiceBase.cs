﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MetaTrader.Core.ViewModels;
using MvvmCross.Core.Views;

namespace MetaTrader.Core.Services
{
    public abstract class AppServiceBase : IAppService
    {
        protected int mCountClose = 0;
		protected Type mCloseViewToViewModel = null;

		public void CloseView(int countClose = 1, Action action = null)
        {
            mCountClose = mCountClose + countClose;
            CloseViewIfCan(action);
        }

		public void CloseViewToViewModel(Type viewModelType, Action action = null)
		{
			mCloseViewToViewModel = viewModelType;
			CloseViewIfCan(action);
		}

		protected abstract void CloseViewIfCan(Action action = null);

        public abstract void DismissProgressDialog();

        public abstract void ShowDialog(string message);

        public abstract void ShowProgressDialog(string message);

        public abstract void ShowText(string message);
    }
}
