﻿using MetaTrader.Core.Models;
using MetaTrader.Core.ViewModels;
using MvvmCross.Platform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MetaTrader.Core.Services
{
    public class MetaTraderService
    {
        //NativeMessageHandler mNativeMessageHandler;
        private HttpClient mHttpClient;
        private Encoding mEncoding;
		private string mLoginId_ORDERATHOME = string.Empty;
		private string mPassword_ORDERATHOME = string.Empty;
		private string mCode_ORDERATHOME = string.Empty;
		private string mLoginId_TURKDEX = "12208";
		private string mPassword_TURKDEX = "159951";
		private string mCode_TURKDEX = "159951";

        public MetaTraderService()
        {
            this.mEncoding = Encoding.GetEncoding("iso-8859-9");
			this.mHttpClient = new HttpClient(new HttpClientHandler());
        }

        private void CallApiAndLoadModel(ResponseMessage responseMessage, string url)
        {
            try
            {
                int i = 0;
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                while (i < 3 && result.StatusCode == HttpStatusCode.InternalServerError)
                {
					result = mHttpClient.GetAsync(url).Result;
                    i++;
                }
                var contentBytes = result.Content.ReadAsByteArrayAsync().Result;
                var content = mEncoding.GetString(contentBytes, 0, contentBytes.Length);
                responseMessage.LoadModel(content);
            }
            catch (Exception ex)
            {
                responseMessage.IsSuccess = false;
                responseMessage.MSG = ex.Message;
            }
        }

		public LoginResponse Login(string loginId, string password, string code)
        //public LoginResponse Login(string loginId, string password, string code)
        {
            var loginResponse = new LoginResponse();
			string urlLogin = string.Format("{0}IDBTrade.asp?KOMUT=HESAPLISTESI&musteriNo={1}&Sifre={2}&rumuz={3}", 
			                                AppConstants.ROOT_API_META_TRADER_TURKDEX, 
			                                loginId, 
			                                password, 
			                                code);
			//string urlLogin = string.Format("{0}IDBTrade.asp?KOMUT=HESAPLISTESI&musteriNo={1}&Sifre={2}&rumuz={3}", AppConstants.ROOT_API_META_TRADER_TURKDEX, loginId, password, code);
            this.CallApiAndLoadModel(loginResponse, urlLogin);

			mLoginId_ORDERATHOME = loginId;
            mPassword_ORDERATHOME = password;
            mCode_ORDERATHOME = code;
			mLoginId_TURKDEX = loginId;
			mPassword_TURKDEX = password;
			mCode_TURKDEX = code;

			return loginResponse;
        }

        public StockResponse GetStock(string name)
        {
            var stockResponse = new StockResponse();
			string urlStock = string.Format("{0}IDBTradePHC.asp?KOMUT=HISSEBILGI&HISSE={1}&musteriNo={2}&Sifre={3}&rumuz={4}", 
			                                AppConstants.ROOT_API_META_TRADER_TURKDEX, 
			                                name, 
			                                mLoginId_TURKDEX, 
			                                mPassword_TURKDEX, 
			                                mCode_TURKDEX);
            this.CallApiAndLoadModel(stockResponse, urlStock);
            return stockResponse;
        }

        public StocksResponse GetStocks()
        {
            var stocksResponse = new StocksResponse();
            string urlStocks = string.Format("{0}IDBTradephc.asp?KOMUT=HISSELISTE&musteriNo={1}&Sifre={2}&rumuz={3}", 
			                                 AppConstants.ROOT_API_META_TRADER_TURKDEX, 
			                                 mLoginId_TURKDEX, 
			                                 mPassword_TURKDEX, 
			                                 mCode_TURKDEX);
            this.CallApiAndLoadModel(stocksResponse, urlStocks);

//			for (int i = 1; i < 10; i++) {
//				Stock stock = new Stock ();
//				stock.Name = "stock " + i;
//				stock.Price = i;
//				stocksResponse.Stocks.Add (stock);
//			}

            return stocksResponse;
        }

		public StockRecordsResponse GetStockRecords(string name)
		{
			var stockRecordsResponse = new StockRecordsResponse();
			string urlStockRecords = string.Format("{0}IDBTradePHC.asp?KOMUT=HISSECHART&HISSE={1}&musteriNo={2}&Sifre={3}&rumuz={4}", 
			                                       AppConstants.ROOT_API_META_TRADER_TURKDEX, 
			                                       name, 
			                                       mLoginId_TURKDEX, 
			                                       mPassword_TURKDEX, 
			                                       mCode_TURKDEX);
			this.CallApiAndLoadModel(stockRecordsResponse, urlStockRecords);
			return stockRecordsResponse;
		}

		public TradeResponse TradeStock(bool isbuy, string name, int amount, double price, OrderType orderType, OrderMethod orderMethod)
		{
			var orderTypeValue = string.Empty;
			var orderMethodValue = string.Empty;

			switch (orderMethod)
			{
				case OrderMethod.Gunluk:
					orderMethodValue = "GUN";
					break;
				case OrderMethod.IMB:
					orderMethodValue = "IMB";
					break;
				case OrderMethod.KIE:
					orderMethodValue = "KIE";
					break;
			}

			switch (orderType)
			{
				case OrderType.Dengeleyici:
					orderTypeValue = "PYS";
					price = 0;
					break;
				case OrderType.Piyasa:
					orderTypeValue = "PYS";
					price = 0;
					break;
				case OrderType.PiyasadanLimite:
					price = 0;
					orderTypeValue = "PKP";
					break;
				case OrderType.Limit:
					orderTypeValue = "LMT";
					break;
			}

			var tradeResponse = new TradeResponse();
			string urlTrade = string.Format("{0}IDBTrade.asp?KOMUT={1}&musteriNo={2}&Sifre={3}&rumuz={4}&HISSE={5}&MIKTAR={6}&FIYAT={7}&ORDER_TYPE={8}&SURE={9}", 
			                                AppConstants.ROOT_API_META_TRADER_TURKDEX, 
			                                isbuy ? "HISSEAL" : "HISSESAT", 
			                                mLoginId_TURKDEX, 
			                                mPassword_TURKDEX, 
			                                mCode_TURKDEX, 
			                                name, 
			                                amount, 
			                                price, 
			                                orderTypeValue, 
			                                orderMethodValue);
			this.CallApiAndLoadModel(tradeResponse, urlTrade);
			return tradeResponse;
		}

		public PozisyonlarsResponse GetPozisyonlars()
		{
			var pozisyonlarsResponse = new PozisyonlarsResponse();
			string urlPozisyonlars = string.Format("{0}IDBTrade.asp?KOMUT=HESAPPORTFOY&musteriNo={1}&Sifre={2}&rumuz={3}", 
			                                       AppConstants.ROOT_API_META_TRADER_TURKDEX, 
			                                       mLoginId_ORDERATHOME, 
			                                       mPassword_ORDERATHOME, 
			                                       mCode_ORDERATHOME);
			this.CallApiAndLoadModel(pozisyonlarsResponse, urlPozisyonlars);
			return pozisyonlarsResponse;
		}

		public HesapOzetisResponse GetHesapOzetis()
		{
			var hesapOzetisResponse = new HesapOzetisResponse();
			string urlHesapOzetis = string.Format("{0}IDBTrade.asp?KOMUT=HESAPBILGI&musteriNo={1}&Sifre={2}&rumuz={3}", 
			                                      AppConstants.ROOT_API_META_TRADER_TURKDEX, 
			                                      mLoginId_ORDERATHOME, 
			                                      mPassword_ORDERATHOME, 
			                                      mCode_ORDERATHOME);
			this.CallApiAndLoadModel(hesapOzetisResponse, urlHesapOzetis);
			return hesapOzetisResponse;
		}

		public VIOPsResponse GetVIOPs()
		{
			var stocksResponse = new VIOPsResponse();
			string urlStocks = string.Format("{0}IDBTradephc.asp?KOMUT=SOZLESMELER&musteriNo={1}&Sifre={2}&rumuz={3}", 
			                                 AppConstants.ROOT_API_META_TRADER_TURKDEX, 
			                                 mLoginId_TURKDEX, 
			                                 mPassword_TURKDEX, 
			                                 mCode_TURKDEX);
			this.CallApiAndLoadModel(stocksResponse, urlStocks);
			return stocksResponse;
		}

		public VIOPResponse GetVIOP(string code)
		{
			var viopResponse = new VIOPResponse();
			string urlStock = string.Format("{0}IDBTradePHC.asp?KOMUT=SOZLESMEBILGI&Sozlesme={1}&musteriNo={2}&Sifre={3}&rumuz={4}", 
			                                AppConstants.ROOT_API_META_TRADER_TURKDEX, 
			                                code, 
			                                mLoginId_TURKDEX, 
			                                mPassword_TURKDEX, 
			                                mCode_TURKDEX);
			this.CallApiAndLoadModel(viopResponse, urlStock);
			return viopResponse;
		}

		public VIOPRecordsResponse GetVIOPRecords(string code)
		{
			var viopRecordsResponse = new VIOPRecordsResponse();
			string urlStockRecords = string.Format("{0}IDBTradePHC.asp?KOMUT=VIOPCHART&Sozlesme={1}&musteriNo={2}&Sifre={3}&rumuz={4}", 
			                                       AppConstants.ROOT_API_META_TRADER_TURKDEX, 
			                                       code, 
			                                       mLoginId_TURKDEX, 
			                                       mPassword_TURKDEX, 
			                                       mCode_TURKDEX);
			this.CallApiAndLoadModel(viopRecordsResponse, urlStockRecords);
			return viopRecordsResponse;
		}

		public TradeResponse TradeVIOP(bool isbuy, string code, int amount, double price, 
		                               FiyatTipi fiyatTipi, EmirTipi emirTipi, Sure sure, string tarih)
		{
			var fiyatTipiPara = string.Empty;
			var emirTipiPara = string.Empty;
			var surePara = string.Empty;

			switch (fiyatTipi)
			{
				case FiyatTipi.EnIyiFiyat:
					fiyatTipiPara = "EIF";
					price = 0;
					break;
				case FiyatTipi.Limitli:
					fiyatTipiPara = "LMT";
					break;
				case FiyatTipi.Piyasa:
					fiyatTipiPara = "PYS";
					price = 0;
					break;
			}

			switch (emirTipi)
			{
				case EmirTipi.GerceklesmezseIptalEt:
					emirTipiPara = "GIE";
					break;
				case EmirTipi.KalaniIptalEt:
					emirTipiPara = "KIE";
					break;
				case EmirTipi.KalaniPasifeYaz:
					emirTipiPara = "KPY";
					break;
			}

			switch (sure)
			{
				case Sure.Gunluk:
					surePara = "GUN";
					tarih = string.Empty;
					break;
				case Sure.IptaleKadarGecerli:
					surePara = "IKG";
					tarih = string.Empty;
					break;
				case Sure.Tarihli:
					surePara = "TAR";
					break;
			}

			var tradeResponse = new TradeResponse();
			string urlTrade = string.Format("{0}IDBTrade.asp?KOMUT=VOBEMIR" +
			                                "&Islem={1}" +
			                                "&MusteriNo={2}&Sifre={3}&rumuz={4}&Sozlesme={5}&Miktar={6}&LimitFiyat={7}&EmirTipi={8}&FiyatTipi={9}&Sure={10}",
			                                AppConstants.ROOT_API_META_TRADER_TURKDEX, 
			                                isbuy ? "A" : "S", 
			                                mLoginId_TURKDEX, 
			                                mPassword_TURKDEX, 
			                                mCode_TURKDEX,
			                                code, 
			                                amount, 
			                                price, 
			                                emirTipiPara, 
				                            fiyatTipiPara,
			                                surePara)
			                        + (string.IsNullOrEmpty(tarih)? string.Empty : ("&Tarih=" + tarih));
			this.CallApiAndLoadModel(tradeResponse, urlTrade);
			return tradeResponse;
		}

    }
}
