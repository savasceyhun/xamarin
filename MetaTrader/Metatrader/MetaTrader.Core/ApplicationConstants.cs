﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetaTrader.Core
{
    public static class AppConstants
    {
        public static readonly string ROOT_API_META_TRADER_TURKDEX = "https://turkdex.phillip.com.tr/entegrasyon/";
		public static readonly string ROOT_API_META_TRADER_ORDERATHOME = "https://orderathome.phillipcapital.com.tr/entegrasyon/";

    }
}
