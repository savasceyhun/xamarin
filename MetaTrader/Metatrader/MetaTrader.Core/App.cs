﻿using MetaTrader.Core.Services;
using MetaTrader.Core.ViewModels;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetaTrader.Core
{
    public class App : MvxApplication
    {
        public App(IAppService appService)
        {            
            Mvx.RegisterSingleton<IAppService>(() => appService);
            Mvx.RegisterSingleton<MetaTraderService>(()=> new MetaTraderService());
			Mvx.RegisterSingleton<IMvxAppStart>(new MvxAppStart<LoginViewModel>());
        }
    }
}
