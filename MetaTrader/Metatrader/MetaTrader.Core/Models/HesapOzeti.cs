﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetaTrader.Core.Models
{
    public class HesapOzeti
    {
		public string Code { get; set; }
		public double AMount { get; set; }
        public string Description { get; set; }
    }
}
