﻿using System;
using System.Collections.Generic;
using MetaTrader.Core.Models;

namespace MetaTrader.Core
{
	public class VIOPsResponse : ResponseMessage
	{
		public string ISL { get; set; }

		private List<VIOP> mVIOPs = new List<VIOP>();
		public List<VIOP> VIOPs
		{
			get
			{
				return mVIOPs;
			}
			set
			{
				mVIOPs = value;
			}
		}

		public VIOPsResponse()
		{
		}

		public override void LoadModel(string content)
		{
			mVIOPs = new List<VIOP>();
			base.LoadModel(content);
			var lines = content.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
			for (int i = 2; i < lines.Length; i++)
			{
				var line = lines[i];
				if (line.StartsWith("|"))
				{
					try
					{
						VIOP viop = new VIOP();
						viop.Name = line.Substring(1, line.IndexOf(":") - 1).Trim();
						var prices = line.Substring(line.IndexOf(":") + 1).Trim().Split(new string[] { ":" }, StringSplitOptions.RemoveEmptyEntries);
						viop.AlisFiyat = double.Parse(prices[0]);
						viop.AlisDerinlik = double.Parse(prices[2]);
						viop.SatisFiyat = double.Parse(prices[1]);
						viop.SatisDerinlik = double.Parse(prices[3]);
						viop.Code = prices[4];
						mVIOPs.Add(viop);
					}
					catch (Exception)
					{
					}
				}
			}
		}
	}
}

