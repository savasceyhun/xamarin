﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetaTrader.Core.Models
{
    public class Pozisyonlar
    {
		public double HesapNumarası { get; set; }
		public string HesapTuru { get; set; }
        public string MenkulAdi { get; set; }
		public double Kapanis { get; set; }
        public double OverallMiktar { get; set; }
        public double Tutar { get; set; }
        public double Maliyet { get; set; }
        public double SatilabilirMiktar { get; set; }
        public double TGunuStokBakiye { get; set; }
        public double T1GunuStokBakiye { get; set; }
        public double TGunuTutar { get; set; }
        public double T1GunuTutar { get; set; }
    }
}
