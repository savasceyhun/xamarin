﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetaTrader.Core.Models
{
    public class HesapOzetisResponse : ResponseMessage
    {
		private List<HesapOzeti> mHesapOzetis = new List<HesapOzeti>();
		public List<HesapOzeti> HesapOzetis
        {
            get
            {
                return mHesapOzetis;
            }
            set
            {
                mHesapOzetis = value;
            }
        }

        public override void LoadModel(string content)
        {
            mHesapOzetis = new List<HesapOzeti>();
            base.LoadModel(content);
            var lines = content.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 2; i < lines.Length; i++)
            {
				var line = lines[i].Trim();
				if(!string.IsNullOrEmpty(line))
				{
					try
					{
						var hesapOzeti = new HesapOzeti();
						hesapOzeti.Code = line.Substring(0, line.IndexOf(":") - 1).Trim();
						var values = line.Substring(line.IndexOf(":") + 1).Trim().Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
						hesapOzeti.AMount = double.Parse(values[0]);
						hesapOzeti.Description = values[1];
						mHesapOzetis.Add(hesapOzeti);
					}
					catch (Exception)
					{
					}
				}
			}
        }
    }
}
