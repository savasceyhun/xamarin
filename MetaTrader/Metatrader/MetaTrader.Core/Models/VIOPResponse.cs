﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetaTrader.Core.Models
{
    public class VIOPResponse : ResponseMessage
    {
		public string ISL { get; set; }

		private VIOP mVIOP = new VIOP();
		public VIOP VIOP
		{
			get
			{
				return mVIOP;
			}
			set
			{
				mVIOP = value;
			}
		}

		public override void LoadModel(string content)
		{
			mVIOP = new VIOP();
			base.LoadModel(content);
			var lines = content.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
			for (int i = 2; i < lines.Length; i++)
			{
				var line = lines[i];
				if (line.StartsWith("|"))
				{
					try
					{
						mVIOP.Name = line.Substring(1, line.IndexOf(":") - 1).Trim();
						var infos = line.Substring(line.IndexOf(":") + 1).Trim().Split(new string[] { ":" }, StringSplitOptions.RemoveEmptyEntries);
						mVIOP.AlisFiyat = double.Parse(infos[0]);
						mVIOP.SatisFiyat = double.Parse(infos[1]);
						mVIOP.AlisDerinlik = double.Parse(infos[2]);
						mVIOP.SatisDerinlik = double.Parse(infos[3]);
						mVIOP.TavanFiyat = double.Parse(infos[4]);
						mVIOP.TabanFiyat = double.Parse(infos[5]);
						mVIOP.TickSize = double.Parse(infos[6]);
						mVIOP.Code = infos[7];
					}
					catch (Exception)
					{
					}
				}
			}
		}


	}
}
