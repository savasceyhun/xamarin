﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetaTrader.Core.Models
{
    public class Stock
    {
        public string Name { get; set; }
        public double CeilingPrice { get; set; }
        public double BasePrice { get; set; }
        public double LastPrice { get; set; }
        public double LowestPrice { get; set; }
        public double HighestPrice { get; set; }
        public double AveragePrice { get; set; }
        public double Ask { get; set; }
        public double Bid { get; set; }
        public string BIG { get; set; }
        public int BRT { get; set; }
        public string OperationDate { get; set; }
        public string ValorDate { get; set; }

        private double mTickValue;
        public double TickValue
        {
            get
            {
                return Math.Max(mTickValue, 0.01);
            }
            set
            {
                mTickValue = value;
            }
        }

        public DateTime Date { get; set; }
    }
}
