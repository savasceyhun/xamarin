﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetaTrader.Core.Models
{
    public class StockResponse : ResponseMessage
    {
        public double SYU { get; set; }
        public double SYL { get; set; }
        public double SYK { get; set; }
        public double SYP { get; set; }
        public string SYT { get; set; }
        public string SYV { get; set; }
        public double SYA { get; set; }
        public double SYS { get; set; }
        public string BIG { get; set; }
        public int BRT { get; set; }
    }
}
