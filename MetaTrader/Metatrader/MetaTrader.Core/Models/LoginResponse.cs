﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetaTrader.Core.Models
{
    public class LoginResponse : ResponseMessage
    {
        public string HSC { get; set; }
        public string HSN { get; set; }
        public string TEMS { get; set; }
        public string SESS { get; set; }
    }
}
