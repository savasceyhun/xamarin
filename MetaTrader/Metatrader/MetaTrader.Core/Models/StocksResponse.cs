﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetaTrader.Core.Models
{
    public class StocksResponse : ResponseMessage
    {
		private List<Stock> mStocks = new List<Stock>();
        public List<Stock> Stocks
        {
            get
            {
                return mStocks;
            }
            set
            {
                mStocks = value;
            }
        }

        public string ISL { get; set; }

        public override void LoadModel(string content)
        {
            mStocks = new List<Stock>();
            base.LoadModel(content);
            var lines = content.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 2; i < lines.Length; i++)
            {
                var line = lines[i];
                if (line.StartsWith("|"))
                {
                    try
                    {
                        Stock stock = new Stock();
                        stock.Name = line.Substring(1, line.IndexOf(":") - 1).Trim();
                        var prices = line.Substring(line.IndexOf(":") + 1).Trim().Split(new string[] { ":" }, StringSplitOptions.RemoveEmptyEntries);
                        stock.LastPrice = double.Parse(prices[0]);
                        stock.LowestPrice = double.Parse(prices[1]);
                        stock.HighestPrice = double.Parse(prices[2]);
                        stock.AveragePrice = double.Parse(prices[3]);
                        mStocks.Add(stock);
                    }
                    catch (Exception)
                    {
                    }
                }                 
            }
        }
    }
}
