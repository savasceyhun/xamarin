﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetaTrader.Core.Models
{
    public class PozisyonlarsResponse : ResponseMessage
    {
		private List<Pozisyonlar> mPozisyonlars = new List<Pozisyonlar>();
		public List<Pozisyonlar> Pozisyonlars
        {
            get
            {
                return mPozisyonlars;
            }
            set
            {
                mPozisyonlars = value;
            }
        }

        public string ISL { get; set; }

        public override void LoadModel(string content)
        {
            mPozisyonlars = new List<Pozisyonlar>();
            base.LoadModel(content);
            var lines = content.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 2; i < lines.Length; i++)
            {
				var line = lines[i].Trim();
				var hsp = "HSP:";
				if (line.StartsWith(hsp))
                {
                    try
                    {
						var pozisyonlar = new Pozisyonlar();
						var values = line.Substring(hsp.Length).Trim().Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
						pozisyonlar.HesapNumarası = double.Parse(values[0]);
						pozisyonlar.HesapTuru = values[1];
						pozisyonlar.MenkulAdi = values[2];
						pozisyonlar.Kapanis = double.Parse(values[3]);
						pozisyonlar.OverallMiktar = double.Parse(values[4]);
						pozisyonlar.Tutar = double.Parse(values[5]);
						pozisyonlar.Maliyet = double.Parse(values[6]);
						pozisyonlar.SatilabilirMiktar = double.Parse(values[7]);
						pozisyonlar.TGunuStokBakiye = double.Parse(values[8]);
						pozisyonlar.T1GunuStokBakiye = double.Parse(values[9]);
						pozisyonlar.TGunuTutar = double.Parse(values[10]);
						pozisyonlar.T1GunuTutar = double.Parse(values[11]);
                        Pozisyonlars.Add(pozisyonlar);
                    }
                    catch (Exception)
                    {
                    }
                }                 
            }
        }
    }
}
