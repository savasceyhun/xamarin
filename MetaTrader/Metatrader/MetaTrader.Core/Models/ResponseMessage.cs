﻿using MvvmCross.Platform;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetaTrader.Core.Models
{
    public class ResponseMessage
    {
        public bool IsSuccess { get; set; }
        public string MSG { get; set; }

        public virtual void LoadModel(string content)
        {
            var lines = content.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
            var properties = this.GetType().GetProperties().Where(t => t.PropertyType == typeof(string) || IsNumeric(t.PropertyType) ).ToList();
            this.IsSuccess = lines[0].Trim() == "OK";
            for (int i = 1; i < lines.Length; i++)
            {
                var line = lines[i];
                var property = properties.FirstOrDefault(t => line.StartsWith(t.Name + ":"));
                if (property != null)
                {
                    var value = string.Empty;
                    if (line.Length > 0)
                    {
                        value = line.Substring(property.Name.Length + 1).Trim();
                    }
                    if(IsNumeric(property.PropertyType))
                    {
                        try
                        {
                            property.SetValue(this, Convert.ChangeType(value, property.PropertyType));
                        }
                        catch (Exception) { }                        
                    }
                    else 
                    {
                        property.SetValue(this, value);
                    }
                }
            }
        }

        private bool IsNumeric(Type dataType)
        {
            if (dataType == null)
                throw new ArgumentNullException("dataType");

            return (dataType == typeof(int)
                    || dataType == typeof(double)
                    || dataType == typeof(long)
                    || dataType == typeof(short)
                    || dataType == typeof(float)
                    || dataType == typeof(Int16)
                    || dataType == typeof(Int32)
                    || dataType == typeof(Int64)
                    || dataType == typeof(uint)
                    || dataType == typeof(UInt16)
                    || dataType == typeof(UInt32)
                    || dataType == typeof(UInt64)
                    || dataType == typeof(sbyte)
                    || dataType == typeof(Single)
                   );
        }
    }
}
