﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetaTrader.Core.Models
{
    public class VIOPRecordsResponse : ResponseMessage
    {
        public string Name { get; set; }

        private List<double> mPrices = new List<double>();
        public List<double> Prices
        {
            get
            {
                return mPrices;
            }
            set
            {
                mPrices = value;
            }
        }

        public override void LoadModel(string content)
        {
            mPrices = new List<double>();
            base.LoadModel(content);

			var lines = content.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);

			var msgSuccess = lines[0].Trim();
			this.IsSuccess = msgSuccess.EndsWith("OK");
			if (this.IsSuccess)
			{
				this.Name = msgSuccess.Substring(0, msgSuccess.Length - 2);
			}

			for (int i = 2; i < lines.Length; i++)
            {
                var line = lines[i];
                if (line.StartsWith("|"))
                {
                    var price = double.Parse(line.Substring(1).Trim());
                    mPrices.Add(price);
                }
            }
        }
    }
}
