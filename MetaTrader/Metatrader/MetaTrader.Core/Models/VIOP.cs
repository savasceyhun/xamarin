﻿using System;
namespace MetaTrader.Core
{
	public class VIOP
	{
		public string Name { get; set; }
		public string Code { get; set; }

		public double AlisFiyat { get; set; } //Bid
		public double AlisDerinlik { get; set; } //Bid Amount
		public double SatisFiyat { get; set; } //Ask
		public double SatisDerinlik { get; set; } //Ask Amount
		public double TavanFiyat { get; set; } //Ceiling Price
		public double TabanFiyat  { get; set; } //Base Price

		public double TickSize { get; set; }

		public DateTime Date { get; set; }


		public VIOP()
		{
			Date = DateTime.Now;
		}
	}
}

