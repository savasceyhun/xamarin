using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MvvmCross.Droid.Views;
using MvvmCross.Platform;
using MetaTrader.Core.Services;
using MetaTrader.Core.ViewModels;

namespace MetaTrader.UI.Droid.Views
{
    public abstract class MvxActivityBase : MvxActivity
    {
        public override void Finish()
        {
            if(this.ViewModel is ViewModelBase)
            {
                (this.ViewModel as ViewModelBase).OnFinish();
            }
            base.Finish();
        }
    }
}