using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using MvvmCross.Droid.Simple;
using MetaTrader.Core.ViewModels;
using Android.OS;
using MvvmCross.Droid.Views;

namespace MetaTrader.UI.Droid.Views
{
    [Activity(ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class HomeView : MvxActivityBase
    {
        protected override void OnViewModelSet()
        {
            SetContentView(Resource.Layout.ViewHome);
        }

        public override void Finish()
        {
            base.Finish();
        }
    }
}