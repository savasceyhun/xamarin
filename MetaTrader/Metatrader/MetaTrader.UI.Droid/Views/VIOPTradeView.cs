using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using MvvmCross.Droid.Simple;
using MetaTrader.Core.ViewModels;
using Android.OS;
using MvvmCross.Droid.Views;
using OxyPlot.Xamarin.Android;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using Android.Widget;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.Binders;
using MvvmCross.Platform;
using Java.Util;

namespace MetaTrader.UI.Droid.Views
{
    [Activity(ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, WindowSoftInputMode = Android.Views.SoftInput.StateHidden)]
    public class VIOPTradeView : MvxActivityBase
    {
        private Timer mTimer;
        private int mPeriod = 1 * 1 * 1000;
        private GetVIOPTimerTask mGetStockTimerTask;

        public VIOPTradeViewModel VIOPTradeViewModel
        {
            get
            {
                return (this as MvxActivityBase).ViewModel as VIOPTradeViewModel;
            }
        }

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            TabHost tabhost = FindViewById<TabHost>(Android.Resource.Id.TabHost);
            tabhost.Setup();

            //Tab 1
            TabHost.TabSpec spec = tabhost.NewTabSpec("Grafik");
            spec.SetContent(Resource.Id.tab1);
            spec.SetIndicator("Grafik");            
            tabhost.AddTab(spec);

            //Tab 2
            spec = tabhost.NewTabSpec("Bilgi");
            spec.SetContent(Resource.Id.tab2);
            spec.SetIndicator("Bilgi");
            tabhost.AddTab(spec);

            for (int i = 0; i < tabhost.TabWidget.ChildCount; i++)
            {
                tabhost.TabWidget.GetChildAt(i).SetBackgroundResource(Resource.Drawable.tab_indicator_default);
            }


			FindViewById<EditText>(Resource.Id.et_tarih).Click += (sender, e) =>
			{

				DateTime datetime = DateTime.Now;
				DateTime.TryParseExact(FindViewById<EditText>(Resource.Id.et_tarih).Text, "dd/MM/yyyy",
				                       System.Globalization.CultureInfo.InvariantCulture
				                       , System.Globalization.DateTimeStyles.AdjustToUniversal
				                       , out datetime);

				new DatePickerDialog(this, (sender2, e2) => 
				{
					FindViewById<EditText>(Resource.Id.et_tarih).Text = e2.Date.ToString("dd/MM/yyyy");
				}, datetime.Year, datetime.Month - 1, datetime.Day).Show();
			};
        }

        protected override void OnViewModelSet()
        {
			SetContentView(Resource.Layout.ViewVIOPTrade);

            //Set Chart DefaultFontSize
			this.VIOPTradeViewModel.PlotModel.DefaultFontSize = this.Resources.GetDimensionPixelSize(Resource.Dimension.text_Size_S3);

            //Binding Chart Model
            var plotView = this.FindViewById<PlotView>(Resource.Id.plotview);
            var set = this.CreateBindingSet<VIOPTradeView, VIOPTradeViewModel>();
            set.Bind(plotView).For(v => v.Model)
                                .To(vm => vm.PlotModel);
            set.Apply();
        }

        protected override void OnResume()
        {
            base.OnResume();
            if (mTimer == null)
            {
                mTimer = new Timer();
                mGetStockTimerTask = new GetVIOPTimerTask(this);
                mTimer.Schedule(mGetStockTimerTask, mPeriod, mPeriod);
            }
        }

        private void CancelTimer()
        {
            if (mTimer != null)
            {
                try
                {
                    mGetStockTimerTask.Cancel();
                    mGetStockTimerTask.Dispose();
                    mTimer.Cancel();
                    mTimer.Dispose();
                    mTimer = null;
                    mGetStockTimerTask = null;
                }
                catch (Exception)
                {
                }

            }
        }

        protected override void OnPause()
        {
            base.OnPause();
            CancelTimer();
        }

        protected override void OnStop()
        {
            base.OnStop();
            CancelTimer();
        }

        private class GetVIOPTimerTask : TimerTask
        {
            private VIOPTradeView mVIOPTradeView;
            private bool mIsRunning = false;

            public GetVIOPTimerTask(VIOPTradeView viopTradeView)
            {
                mVIOPTradeView = viopTradeView;
            }

            public override void Run()
            {
                if (!mIsRunning)
                {
                    mIsRunning = true;
					mVIOPTradeView.VIOPTradeViewModel.LoadStockWithoutProgressDialog();
                    mIsRunning = false;
                }
            }
        }
    }
}