using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using MvvmCross.Droid.Simple;
using MetaTrader.Core.ViewModels;
using Android.OS;
using MvvmCross.Droid.Views;
using Java.Util;

namespace MetaTrader.UI.Droid.Views
{
    [Activity(ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class IptalsView : MvxActivityBase
    {
        private Timer mTimer;
        private int mPeriod = 15 * 60 * 1000;
        private GetStocksTimerTask mGetStocksTimerTask;

        protected override void OnViewModelSet()
        {
			SetContentView(Resource.Layout.ViewIptals);
        }

        protected override void OnResume()
        {
            base.OnResume();
            if(mTimer == null)
            {
                mTimer = new Timer();
                mGetStocksTimerTask = new GetStocksTimerTask(this);
                mTimer.Schedule(mGetStocksTimerTask, mPeriod, mPeriod);
            }
        }

        private void CancelTimer()
        {
            if (mTimer != null)
            {
                try
                {
                    mGetStocksTimerTask.Cancel();
                    mGetStocksTimerTask.Dispose();
                    mTimer.Cancel();
                    mTimer.Dispose();
                    mTimer = null;
                    mGetStocksTimerTask = null;
                }
                catch (Exception)
                {
                }

            }
        }

        protected override void OnPause()
        {
            base.OnPause();
            CancelTimer();
        }

        protected override void OnStop()
        {
            base.OnStop();
            CancelTimer();
        }

        private class GetStocksTimerTask : TimerTask
        {
            private MvxActivityBase mMvxActivityBase;
            public GetStocksTimerTask(MvxActivityBase mvxActivityBase)
            {
                mMvxActivityBase = mvxActivityBase;
            }

            public override void Run()
            {
                //(mMvxActivityBase.ViewModel as StocksViewModel).LoadStocks();
            }
        }
    }
}