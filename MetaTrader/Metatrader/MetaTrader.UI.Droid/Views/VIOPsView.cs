﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.Util;
using MetaTrader.Core.ViewModels;
using MetaTrader.UI.Droid.Views;

namespace MetaTrader.UI.Droid
{
	[Activity(ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, WindowSoftInputMode = Android.Views.SoftInput.StateHidden)]
	public class VIOPsView : MvxActivityBase
	{
		private Timer mTimer;
		private int mPeriod = 15 * 60 * 1000;
		private GetVIOPsTimerTask mGetVIOPsTimerTask;

		protected override void OnViewModelSet()
		{
			SetContentView(Resource.Layout.ViewVIOPs);
		}

		protected override void OnResume()
		{
			base.OnResume();
			if (mTimer == null)
			{
				mTimer = new Timer();
				mGetVIOPsTimerTask = new GetVIOPsTimerTask(this);
				mTimer.Schedule(mGetVIOPsTimerTask, mPeriod, mPeriod);
			}
		}

		private void CancelTimer()
		{
			if (mTimer != null)
			{
				try
				{
					mGetVIOPsTimerTask.Cancel();
					mGetVIOPsTimerTask.Dispose();
					mTimer.Cancel();
					mTimer.Dispose();
					mTimer = null;
					mGetVIOPsTimerTask = null;
				}
				catch (Exception)
				{
				}

			}
		}

		protected override void OnPause()
		{
			base.OnPause();
			CancelTimer();
		}

		protected override void OnStop()
		{
			base.OnStop();
			CancelTimer();
		}

		private class GetVIOPsTimerTask : TimerTask
		{
			private MvxActivityBase mMvxActivityBase;
			public GetVIOPsTimerTask(MvxActivityBase mvxActivityBase)
			{
				mMvxActivityBase = mvxActivityBase;
			}

			public override void Run()
			{
				(mMvxActivityBase.ViewModel as VIOPsViewModel).LoadVIOPs();
			}
		}
	}
}

