using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using MvvmCross.Droid.Simple;
using MetaTrader.Core.ViewModels;
using Android.OS;
using MvvmCross.Droid.Views;
using MetaTrader.Core;

namespace MetaTrader.UI.Droid.Views
{
    [Activity(MainLauncher = true, WindowSoftInputMode = Android.Views.SoftInput.AdjustPan
	         , ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class LoginView : MvxActivityBase
    {
        protected override void OnViewModelSet()
        {
            SetContentView(Resource.Layout.ViewLogin);
        }
    }
}