using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using MvvmCross.Droid.Simple;
using MetaTrader.Core.ViewModels;
using Android.OS;
using MvvmCross.Droid.Views;
using OxyPlot.Xamarin.Android;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using Android.Widget;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.Binders;
using Java.Util;

namespace MetaTrader.UI.Droid.Views
{
    [Activity(ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class VIOPConfirmOrderView : MvxActivityBase
    {
        private Timer mTimer;
        private int mPeriod = 1 * 1 * 1000;
        private GetVIOPTimerTask mGetStockTimerTask;

        protected override void OnViewModelSet()
        {
			SetContentView(Resource.Layout.ViewVIOPConfirmOrder);
        }

        protected override void OnResume()
        {
            base.OnResume();
            if (mTimer == null)
            {
                mTimer = new Timer();
                mGetStockTimerTask = new GetVIOPTimerTask(this);
                mTimer.Schedule(mGetStockTimerTask, mPeriod, mPeriod);
            }
        }

        private void CancelTimer()
        {
            if (mTimer != null)
            {
                try
                {
                    mGetStockTimerTask.Cancel();
                    mGetStockTimerTask.Dispose();
                    mTimer.Cancel();
                    mTimer.Dispose();
                    mTimer = null;
                    mGetStockTimerTask = null;
                }
                catch (Exception)
                {
                }

            }
        }

        protected override void OnPause()
        {
            base.OnPause();
            CancelTimer();
        }

        protected override void OnStop()
        {
            base.OnStop();
            CancelTimer();
        }

        private class GetVIOPTimerTask : TimerTask
        {
            private MvxActivityBase mMvxActivityBase;
            private bool mIsRunning = false;

            public GetVIOPTimerTask(MvxActivityBase mvxActivityBase)
            {
                mMvxActivityBase = mvxActivityBase;
            }

            public override void Run()
            {
                if (!mIsRunning)
                {
                    mIsRunning = true;
                    (mMvxActivityBase.ViewModel as VIOPConfirmOrderViewModel).LoadPrice();
                    mIsRunning = false;
                }
            }
        }
    }
}