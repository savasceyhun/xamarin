using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MetaTrader.Core.Services;
using MvvmCross.Core.Views;
using MvvmCross.Core.ViewModels;
using MetaTrader.Core.ViewModels;
using System.Threading.Tasks;

namespace MetaTrader.UI.Droid.Services
{
    public class AppService : AppServiceBase
    {
        private Application mContext;
        private Activity mCurrentActivity = null;
        private bool mClosingView = false;

        public Task<Activity> CurrentActivityNonNull
        {
            get
            {                
                return Task.Run(() => 
                {
					while (mCurrentActivity == null || mCurrentActivity.IsFinishing)
                    {
                        new System.Threading.ManualResetEvent(false).WaitOne(0);
                    };
                    return mCurrentActivity;
                });
            }
        }

        public AppService(Context context)
        {
            mContext = context.ApplicationContext as Application;            
            mContext.RegisterActivityLifecycleCallbacks(new ActivityLifecycleCallbacks(this));
        }

        public override async void ShowDialog(string message)
        {
            (await CurrentActivityNonNull).RunOnUiThread(() =>
            {
                AlertDialog.Builder alert = new AlertDialog.Builder(mCurrentActivity);
                alert.SetTitle(Resource.String.app_name);
                alert.SetMessage(message);
                alert.SetPositiveButton("Ok", (senderAlert, args) => {
                    (senderAlert as AlertDialog).Dismiss();
                });
                mCurrentActivity.RunOnUiThread(() => {
                    alert.Create().Show();
                });
            });
        }

        ProgressDialog mProgressDialog;
        public override async void ShowProgressDialog(string message)
        {
            (await CurrentActivityNonNull).RunOnUiThread(() =>
            {
                DismissProgressDialog();
                mProgressDialog = new ProgressDialog(mCurrentActivity);
                mProgressDialog.SetMessage(message);
                mProgressDialog.SetCancelable(false);
                mProgressDialog.Show();
            });
        }

        public override async void DismissProgressDialog()
        {
            ProgressDialog progressDialog = mProgressDialog;
            (await CurrentActivityNonNull).RunOnUiThread(() =>
            {
                if (progressDialog != null)
                {
                    progressDialog.Dismiss();
                    if(progressDialog == mProgressDialog)
                    {
                        progressDialog = null;
                    }                    
                }
            });
        }

        public override void ShowText(string message)
        {
            Toast.MakeText(mContext, message, ToastLength.Long).Show();
        }

		protected override async void CloseViewIfCan(Action action = null)
		{
			var activity = (await CurrentActivityNonNull);
			if (mCloseViewToViewModel != null)
			{
				if ((!(activity is IMvxView)) || (activity as IMvxView).ViewModel.GetType() != mCloseViewToViewModel)
				{
					mCountClose = 1;
				}
				else
				{
					mCloseViewToViewModel = null;
				}
			}
			if (mCountClose > 0)
			{
				if (!mClosingView)
				{
					mClosingView = true;
					if (activity is IMvxView && (activity as IMvxView).ViewModel is MvxNavigatingObject)
					{
						try
						{
							var viewModel = (activity as IMvxView).ViewModel as MvxNavigatingObject;
							var close = viewModel.GetType().GetMethod("Close", System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance, null, new[] { typeof(IMvxViewModel) }, null);
							close.Invoke(viewModel, new object[] { viewModel });
						}
						catch (Exception)
						{
							activity.Finish();
						}
					}
					else
					{
						activity.Finish();
					}
					mCountClose--;
					mClosingView = false;
					CloseViewIfCan(action);
				}
			}
			else
			{
				if (action != null)
				{
					action.Invoke();
				}
			}
        }

		private class ActivityLifecycleCallbacks : Java.Lang.Object, Application.IActivityLifecycleCallbacks
        {
			AppService mAppService;
            public ActivityLifecycleCallbacks(AppService messageService)
            {
                mAppService = messageService;
            }

            public void OnActivityCreated(Activity activity, Bundle savedInstanceState)
            {
                mAppService.mCurrentActivity = activity;   
            }

            public void OnActivityStarted(Activity activity)
            {
                //mMessageService.mCurrentActivity = activity;
				//mMessageService.CloseViewIfCan();
            }
            public void OnActivityResumed(Activity activity)
            {
                mAppService.mCurrentActivity = activity;
            }

            public void OnActivityPaused(Activity activity)
            {
                if (mAppService.mCurrentActivity == activity)
                {
                    mAppService.mCurrentActivity = null;
                }
            }

            public void OnActivityStopped(Activity activity)
            {
                if (mAppService.mCurrentActivity == activity)
                {
                    mAppService.mCurrentActivity = null;
                }
            }

            public void OnActivityDestroyed(Activity activity)
            {
                if (mAppService.mCurrentActivity == activity)
                {
                    mAppService.mCurrentActivity = null;
                }
            }

            public void OnActivitySaveInstanceState(Activity activity, Bundle outState)
            {
            }
        }
    }
}