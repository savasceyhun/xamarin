﻿using MvvmCross.Core.ViewModels;
using MvvmCross.Droid.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.Content;

namespace MetaTrader.Core.ViewModels
{
    public class CustomMvxAndroidViewPresenter : MvxAndroidViewPresenter
    {
        public override void Close(IMvxViewModel viewModel)
        {
            base.Close(viewModel);            
        }
    }
}
