using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Graphics.Drawables;

namespace MetaTrader.UI.Droid.Widgets
{
    public class ImageButtonScaleHeight : ImageView
    {
        public ImageButtonScaleHeight(Context context) : base(context)
        {
        }

        public ImageButtonScaleHeight(Context context, IAttributeSet attrs) : base(context, attrs)
        {
        }

        public ImageButtonScaleHeight(Context context, IAttributeSet attrs, int defStyleAttr) : base(context, attrs, defStyleAttr)
        {
        }

        public ImageButtonScaleHeight(Context context, IAttributeSet attrs, int defStyleAttr, int defStyleRes) : base(context, attrs, defStyleAttr, defStyleRes)
        {
        }

        protected ImageButtonScaleHeight(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
        }

        protected override void OnSizeChanged(int w, int h, int oldw, int oldh)
        {
            base.OnSizeChanged(w, h, oldw, oldh);
            if (this.LayoutParameters.Width != h)
            {
                if (this.Drawable != null)
                {
                    this.LayoutParameters.Width = h * this.Drawable.IntrinsicWidth / this.Drawable.IntrinsicHeight;
                    this.Visibility = ViewStates.Invisible;
                    this.Post(() =>
                    {
                        this.RequestLayout();
                        this.Visibility = ViewStates.Visible;
                    });
                }
            }
        }
    }
}