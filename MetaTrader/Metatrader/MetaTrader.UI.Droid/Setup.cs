using Android.Content;
using Android.Widget;
using Java.Lang;
using MetaTrader.Core;
using MetaTrader.Core.Services;
using MetaTrader.Core.ViewModels;
using MetaTrader.UI.Droid.Services;
using MvvmCross.Core.ViewModels;
using MvvmCross.Droid.Platform;
using MvvmCross.Droid.Views;
using MvvmCross.Platform;
using System.Globalization;
using static Java.Lang.Thread;

namespace MetaTrader.UI.Droid
{
    public class Setup : MvxAndroidSetup
    {
        delegate bool Del(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certificate,
                                    System.Security.Cryptography.X509Certificates.X509Chain chain,
                                    System.Net.Security.SslPolicyErrors sslPolicyErrors);


        public Setup(Context applicationContext) : base(applicationContext)
        {
            Thread.DefaultUncaughtExceptionHandler = new UncaughtExceptionHandler(applicationContext);
            CultureInfo cultureInfo = new CultureInfo(System.Threading.Thread.CurrentThread.CurrentCulture.Name, true);
            cultureInfo.NumberFormat = NumberFormatInfo.InvariantInfo;
            CultureInfo.DefaultThreadCurrentCulture = cultureInfo;
			System.Net.ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) =>
			{
				if (sender is System.Net.HttpWebRequest)
				{
					System.Net.HttpWebRequest httpWebRequest = sender as System.Net.HttpWebRequest;
					if (httpWebRequest.RequestUri.OriginalString.Contains(AppConstants.ROOT_API_META_TRADER_TURKDEX)
					    || httpWebRequest.RequestUri.OriginalString.Contains(AppConstants.ROOT_API_META_TRADER_ORDERATHOME))
					{
						return true;
					}
				}
				return false;
			};
        }

        protected override IMvxAndroidViewPresenter CreateViewPresenter()
        {
            return new CustomMvxAndroidViewPresenter();
        }

        protected override IMvxApplication CreateApp()
        {
            AppService appService = new AppService(this.ApplicationContext);
            return new App(appService);
        }


        public class UncaughtExceptionHandler : Java.Lang.Object, IUncaughtExceptionHandler
        {
            private Context mContext;

            public UncaughtExceptionHandler(Context context)
            {
                mContext = context;
            }

            public void UncaughtException(Thread thread, Throwable ex)
            {
                Toast.MakeText(mContext, ex.Message, ToastLength.Long).Show();
            }
        }

        protected override MvxAndroidLifetimeMonitor CreateLifetimeMonitor()
        {            
            return base.CreateLifetimeMonitor();
        }
    }
}