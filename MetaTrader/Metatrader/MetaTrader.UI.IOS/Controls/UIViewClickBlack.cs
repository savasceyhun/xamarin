using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;
using CoreGraphics;

namespace MetaTrader.UI.IOS
{
	partial class UIViewClickBlack : UIView
	{
		
		UIColor mBackgroundColor;

		public UIViewClickBlack() : base()
		{
			this.InitControl();
		}

		public UIViewClickBlack(CGRect cgRect) : base(cgRect)
		{
			this.InitControl();
		}

		public UIViewClickBlack(NSCoder nsCoder) : base(nsCoder)
		{
			this.InitControl();
		}

		public UIViewClickBlack(NSObjectFlag nsObjectFlag) : base(nsObjectFlag)
		{
			this.InitControl();
		}

		public UIViewClickBlack (IntPtr handle) : base (handle)
		{
			this.InitControl();
		}

		private void InitControl()
		{			
		}

		public override void TouchesBegan (NSSet touches, UIEvent evt)
		{
			mBackgroundColor = this.BackgroundColor;
			this.BackgroundColor = blend(mBackgroundColor, new UIColor(0, 0, 0, 0.196f));

			base.TouchesBegan (touches, evt);
		}

		public override void TouchesEnded (NSSet touches, UIEvent evt)
		{
			this.BackgroundColor = mBackgroundColor;

			base.TouchesEnded (touches, evt);
		}

		public override void TouchesCancelled (NSSet touches, UIEvent evt)
		{				
			this.BackgroundColor = mBackgroundColor;

			base.TouchesCancelled (touches, evt);
		}

		public UIColor blend(UIColor color1, UIColor color2)
		{
			nfloat r1, g1, b1, a1;
			color1.GetRGBA (out r1, out g1, out b1, out a1);

			nfloat r2, g2, b2, a2;
			color2.GetRGBA (out r2, out g2, out b2, out a2);

			nfloat beta = 1 - a2;	

			return new UIColor (r1 * beta + r2 * a2, g1 * beta + g2 * a2, b1 * beta + b2 * a2, 1);
		}
	}
}
