using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;
using CoreGraphics;
using MetaTrader.UI.IOS;

namespace MetaTrader.UI.IOS
{
	partial class UIButtonDefault : UIButton
	{

		public UIButtonDefault() : base()
		{
			this.InitControl();
		}

		public UIButtonDefault(UIButtonType uiButtonType) : base(uiButtonType)
		{
			this.InitControl();
		}

		public UIButtonDefault(CGRect cgRect) : base(cgRect)
		{
			this.InitControl();
		}

		public UIButtonDefault(NSCoder nsCoder) : base(nsCoder)
		{
			this.InitControl();
		}

		public UIButtonDefault(NSObjectFlag nsObjectFlag) : base(nsObjectFlag)
		{
			this.InitControl();
		}

		public UIButtonDefault (IntPtr handle) : base (handle)
		{
			this.InitControl();
		}

		private void InitControl()
		{
			this.AdjustsImageWhenHighlighted = false;
			this.SetBackgroundImage (new UIImage ("ButtonDefault.png"), UIControlState.Normal);
		}

		public override bool Highlighted 
		{
			get 
			{
				return false;
			}
			set
			{
				base.Highlighted = value;
			}
		}

		public override void TouchesBegan (NSSet touches, UIEvent evt)
		{
			this.SetBackgroundImage (new UIImage ("ButtonPress.png"), UIControlState.Normal);
			base.TouchesBegan (touches, evt);
		}

		public override void TouchesEnded (NSSet touches, UIEvent evt)
		{
			this.SetBackgroundImage (new UIImage ("ButtonDefault.png"), UIControlState.Normal);
			base.TouchesEnded (touches, evt);
		}

		public override void TouchesCancelled (NSSet touches, UIEvent evt)
		{				
			this.SetBackgroundImage (new UIImage ("ButtonDefault.png"), UIControlState.Normal);			
			base.TouchesCancelled (touches, evt);
		}
	}
}
