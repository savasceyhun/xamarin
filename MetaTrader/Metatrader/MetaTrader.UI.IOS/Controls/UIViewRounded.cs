using Foundation;
using System;
using UIKit;
using CoreGraphics;

namespace MetaTrader.UI.IOS
{
	partial class UIViewRounded : UIView
	{
		public UIViewRounded() : base()
		{
			InitControl();
		}

		public UIViewRounded(IntPtr handle) : base(handle)
		{
			InitControl();
		}

		public UIViewRounded(CGRect cgRect) : base(cgRect)
		{
			this.InitControl();
		}

		public UIViewRounded(NSCoder nsCoder) : base(nsCoder)
		{
			this.InitControl();
		}

		public UIViewRounded(NSObjectFlag nsObjectFlag) : base(nsObjectFlag)
		{
			this.InitControl();
		}

		private void InitControl()
		{
			this.Layer.BorderColor = UIColor.Black.CGColor;
			this.Layer.BorderWidth = 2;
			this.Layer.CornerRadius = 5;
		}
	}
}