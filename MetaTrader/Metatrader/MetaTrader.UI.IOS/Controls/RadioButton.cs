using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;
using CoreGraphics;

namespace MetaTrader.UI.IOS
{
	partial class RadioButton : UIButton
	{
		private bool mChecked = false;
		public bool Checked
		{
			get 
			{
				return mChecked;
			}
			set 
			{
				mChecked = value;
				loadImage();

				if (CheckedChanged != null)
				{
					CheckedChanged(this, EventArgs.Empty);
				}
			}
		}

		public override bool Enabled 
		{
			get 
			{
				return base.Enabled;
			}
			set 
			{
				base.Enabled = value;
					loadImage ();
			}
		}

		public event EventHandler CheckedChanged;

		public RadioButton() : base()
		{
			this.InitControl();
		}

		public RadioButton(UIButtonType uiButtonType) : base(uiButtonType)
		{
			this.InitControl();
		}

		public RadioButton(CGRect cgRect) : base(cgRect)
		{
			this.InitControl();
		}

		public RadioButton(NSCoder nsCoder) : base(nsCoder)
		{
			this.InitControl();
		}

		public RadioButton(NSObjectFlag nsObjectFlag) : base(nsObjectFlag)
		{
			this.InitControl();
		}

		public RadioButton (IntPtr handle) : base (handle)
		{
			this.InitControl();
		}
			
		private void InitControl()
		{
			this.ImageView.ContentMode = UIViewContentMode.ScaleAspectFit;
			loadImage();
			this.TouchUpInside += (object sender, EventArgs e) => 
			{
				Checked = true;
			};

			this.TitleLabel.LineBreakMode = UILineBreakMode.TailTruncation;
			this.TitleLabel.Lines = 1;
		}

		private void loadImage()
		{
			if (Checked) 
			{
				this.SetImage (new UIImage ("RadioButtonCheck.png").ImageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal), UIControlState.Normal);
			} 
			else 
			{
				if (this.Enabled) 
				{
					this.SetImage (new UIImage ("RadioButtonUncheck.png").ImageWithRenderingMode (UIImageRenderingMode.AlwaysOriginal), UIControlState.Normal);
				}
				else 
				{
					this.SetImage (new UIImage ("RadioButtonDisable.png").ImageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal), UIControlState.Normal);
				}
			}
		}		
			
	}
}
