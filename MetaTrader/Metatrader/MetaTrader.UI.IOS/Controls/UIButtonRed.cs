using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;
using CoreGraphics;

namespace MetaTrader.UI.IOS
{
	partial class UIButtonRed : UIButton
	{

		private string mTitleText = string.Empty;
		public string TitleText
		{
			get 
			{
				return mTitleText;
			}
			set 
			{
				if (TitleText != value) 
				{
					mTitleText = value;
					this.SetTitle (value, UIControlState.Normal);
					this.TitleLabel.SizeToFit();
				}
			}
		}

		public UIButtonRed() : base()
		{
			this.InitControl();
		}

		public UIButtonRed(UIButtonType uiButtonType) : base(uiButtonType)
		{
			this.InitControl();
		}

		public UIButtonRed(CGRect cgRect) : base(cgRect)
		{
			this.InitControl();
		}

		public UIButtonRed(NSCoder nsCoder) : base(nsCoder)
		{
			this.InitControl();
		}

		public UIButtonRed(NSObjectFlag nsObjectFlag) : base(nsObjectFlag)
		{
			this.InitControl();
		}

		public UIButtonRed (IntPtr handle) : base (handle)
		{
			this.InitControl();
		}

		private void InitControl()
		{
			this.AdjustsImageWhenHighlighted = false;

			this.TitleLabel.SizeToFit();
			this.TitleLabel.Lines = 0;
			this.TitleLabel.LineBreakMode = UILineBreakMode.WordWrap;
			this.TitleLabel.TextAlignment = UITextAlignment.Center;
			this.HorizontalAlignment = UIControlContentHorizontalAlignment.Center;
			this.SetBackgroundImage (new UIImage ("ButtonRed.png"), UIControlState.Normal);
		}

		public override void TouchesBegan (NSSet touches, UIEvent evt)
		{
			this.SetBackgroundImage (new UIImage ("ButtonRedPress.png"), UIControlState.Normal);
			base.TouchesBegan (touches, evt);
		}

		public override void TouchesEnded (NSSet touches, UIEvent evt)
		{
			this.SetBackgroundImage (new UIImage ("ButtonRed.png"), UIControlState.Normal);
			base.TouchesEnded (touches, evt);
		}

		public override void TouchesCancelled (NSSet touches, UIEvent evt)
		{				
			this.SetBackgroundImage (new UIImage ("ButtonRed.png"), UIControlState.Normal);			
			base.TouchesCancelled (touches, evt);
		}

		public override bool Highlighted 
		{
			get 
			{
				return false;
			}
			set
			{
				base.Highlighted = value;
			}
		}
	}
}
