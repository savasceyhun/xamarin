using Foundation;
using System;
using UIKit;
using CoreGraphics;

namespace MetaTrader.UI.IOS
{
    public partial class UIViewClickDefault : UIView
    {

		UIColor mBackgroundColor;

		public UIViewClickDefault() : base()
		{
			this.InitControl();
		}

		public UIViewClickDefault(CGRect cgRect) : base(cgRect)
		{
			this.InitControl();
		}

		public UIViewClickDefault(NSCoder nsCoder) : base(nsCoder)
		{
			this.InitControl();
		}

		public UIViewClickDefault(NSObjectFlag nsObjectFlag) : base(nsObjectFlag)
		{
			this.InitControl();
		}

		public UIViewClickDefault(IntPtr handle) : base (handle)
		{
			this.InitControl();
		}

		private void InitControl()
		{
		}

		public override void TouchesBegan(NSSet touches, UIEvent evt)
		{
			mBackgroundColor = this.BackgroundColor;
			this.BackgroundColor = UIColor.FromRGB(26,137,26);

			base.TouchesBegan(touches, evt);
		}

		public override void TouchesEnded(NSSet touches, UIEvent evt)
		{
			this.BackgroundColor = mBackgroundColor;

			base.TouchesEnded(touches, evt);
		}

		public override void TouchesCancelled(NSSet touches, UIEvent evt)
		{
			this.BackgroundColor = mBackgroundColor;

			base.TouchesCancelled(touches, evt);
		}
    }
}