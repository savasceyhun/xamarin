using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;
using CoreGraphics;
using CoreAnimation;

namespace MetaTrader.UI.IOS
{
	partial class UITextViewDefault : UITextField
	{
		int borderWidth = 1;
		CALayer bottomBorder = new CALayer();


		public override bool Highlighted
		{
			get
			{
				return false;
			}
			set
			{
				base.Highlighted = value;
			}
		}

		public UITextViewDefault() : base()
		{
			this.InitControl();
		}

		public UITextViewDefault(CGRect cgRect) : base(cgRect)
		{
			this.InitControl();
		}

		public UITextViewDefault(NSCoder nsCoder) : base(nsCoder)
		{
			this.InitControl();
		}

		public UITextViewDefault(NSObjectFlag nsObjectFlag) : base(nsObjectFlag)
		{
			this.InitControl();
		}

		public UITextViewDefault (IntPtr handle) : base (handle)
		{
			this.InitControl();
		}

		private void InitControl()
		{
			this.BorderStyle = UITextBorderStyle.None;

			bottomBorder.Frame = new CGRect(0, this.Frame.Height - borderWidth, this.Frame.Width, borderWidth);
			bottomBorder.BackgroundColor = UIColor.LightGray.CGColor;

			this.Layer.MasksToBounds = true;
			this.Layer.InsertSublayer(bottomBorder, 0);

			this.AddObserver("bounds", NSKeyValueObservingOptions.Prior, (obj) =>
			{
				bottomBorder.Frame = new CGRect(0, this.Frame.Height - borderWidth, this.Frame.Width, borderWidth);
			});
			this.AddObserver("frame", NSKeyValueObservingOptions.Prior, (obj) =>
			{
				bottomBorder.Frame = new CGRect(0, this.Frame.Height - borderWidth, this.Frame.Width, borderWidth);
			});

			this.EditingDidBegin += (object sender, EventArgs e) => {
				bottomBorder.BackgroundColor = new CGColor (0, 255, 0);
			};

			this.EditingDidEnd += (object sender, EventArgs e) => {
				bottomBorder.BackgroundColor = new CGColor (0, 0, 0);
			};

			this.ShouldReturn = new UITextFieldCondition((t)=>{
				t.ResignFirstResponder();
				return true;
			});
		}
			

//		public override void DidUpdateFocus (UIFocusUpdateContext context, UIFocusAnimationCoordinator coordinator)
//		{
//			base.DidUpdateFocus (context, coordinator);
//
//
//		}
			
	}
}
