using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;
using CoreGraphics;

namespace MetaTrader.UI.IOS
{
	partial class UITabButton : UIButton
	{
		private bool mChecked = false;
		public bool Checked
		{
			get 
			{
				return mChecked;
			}
			set 
			{
				mChecked = value;
				loadState ();
				if (CheckedChanged != null)
				{
					CheckedChanged(this, EventArgs.Empty);
				}
			}
		}

		public event EventHandler CheckedChanged;

		public UITabButton() : base()
		{
			this.InitControl();
		}

		public UITabButton(UIButtonType uiButtonType) : base(uiButtonType)
		{
			this.InitControl();
		}

		public UITabButton(CGRect cgRect) : base(cgRect)
		{
			this.InitControl();
		}

		public UITabButton(NSCoder nsCoder) : base(nsCoder)
		{
			this.InitControl();
		}

		public UITabButton(NSObjectFlag nsObjectFlag) : base(nsObjectFlag)
		{
			this.InitControl();
		}

		public UITabButton (IntPtr handle) : base (handle)
		{
			this.InitControl();
		}

		private void InitControl()
		{
			this.ImageView.ContentMode = UIViewContentMode.ScaleAspectFit;
			loadState ();
			this.TouchUpInside += (object sender, EventArgs e) => 
			{
				Checked = true;
			};
		}

		private void loadState()
		{
			if (Checked) 
			{
				this.BackgroundColor = UIColor.LightGray;
				this.SetTitleColor(UIColor.Blue, UIControlState.Normal);

			} 
			else 
			{
				this.BackgroundColor = UIColor.White;
				this.SetTitleColor(UIColor.Black, UIControlState.Normal);
			}
		}	

	}
}
