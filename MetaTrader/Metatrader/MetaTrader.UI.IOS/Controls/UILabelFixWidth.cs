using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;
using CoreGraphics;

namespace MetaTrader.UI.IOS
{
	partial class UILabelFixWidth : UILabel
	{

		public UILabelFixWidth() : base()
		{
			this.InitControl();
		}

		public UILabelFixWidth(CGRect cgRect) : base(cgRect)
		{
			this.InitControl();
		}

		public UILabelFixWidth(NSCoder nsCoder) : base(nsCoder)
		{
			this.InitControl();
		}

		public UILabelFixWidth(NSObjectFlag nsObjectFlag) : base(nsObjectFlag)
		{
			this.InitControl();
		}

		public UILabelFixWidth (IntPtr handle) : base (handle)
		{
			this.InitControl();
		}


		private void InitControl()
		{
			this.Lines = 1;
			this.AdjustsFontSizeToFitWidth = true;
		}


		public override string Text 
		{
			get
			{
				return base.Text;
			}
			set 
			{
				base.Text = value;
				this.SizeToFit ();
			}
		}
	}
}
