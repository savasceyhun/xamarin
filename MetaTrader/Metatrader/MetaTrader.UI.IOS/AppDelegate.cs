﻿using Foundation;
using MvvmCross.Core.ViewModels;
using MvvmCross.iOS.Platform;
using MvvmCross.iOS.Views.Presenters;
using MvvmCross.Platform;
using UIKit;

namespace MetaTrader.UI.IOS
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the
    // User Interface of the application, as well as listening (and optionally responding) to application events from iOS.
    [Register("AppDelegate")]
    public class AppDelegate : MvxApplicationDelegate
    {
        // class-level declarations
        private UIWindow window;

        //
        // This method is invoked when the application has loaded and is ready to run. In this
        // method you should instantiate the window, load the UI into it and then make the window
        // visible.
        //
        // You have 17 seconds to return from this method, or iOS will terminate your application.
        //
		public override bool FinishedLaunching(UIApplication application, NSDictionary options)
        {
            window = new UIWindow(UIScreen.MainScreen.Bounds);





            var presenter = new MvxIosViewPresenter(this, window);
            var setup = new Setup(this, presenter);
            setup.Initialize();

            var start = Mvx.Resolve<IMvxAppStart>();
            start.Start();

            window.MakeKeyAndVisible();

			if (this.GetVersion(UIDevice.CurrentDevice.SystemVersion) >= 7) 
			{
				application.SetStatusBarStyle (UIStatusBarStyle.Default, false);
				this.window.BackgroundColor = UIColor.White;
				this.window.ClipsToBounds = true;
				this.window.RootViewController.View.Frame = new CoreGraphics.CGRect (0, 20, this.window.Frame.Size.Width, this.window.Frame.Size.Height - 20);
			}

            return true;
        }

		public int GetVersion(string systemVersion)
		{
			int version = 0;
			int.TryParse(UIDevice.CurrentDevice.SystemVersion.Split('.')[0], out version);
			return version;
		}

    }
}


