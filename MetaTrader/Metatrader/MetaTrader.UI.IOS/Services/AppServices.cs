﻿using MetaTrader.Core.Services;
using System;
using System.Collections.Generic;
using System.Text;
using UIKit;
using CoreGraphics;
using System.Drawing;
using MBProgressHUD;
using MvvmCross.iOS.Platform;
using ToastIOS;
using System.Linq;
using MvvmCross.iOS.Views;
using MvvmCross.Core.ViewModels;

namespace MetaTrader.UI.IOS.Services
{
    public class AppServices : AppServiceBase
    {
		private MvxApplicationDelegate mApplicationDelegate;
        private MTMBProgressHUD mMTMBProgressHUD;

        public AppServices(MvxApplicationDelegate applicationDelegate)
		{
			mApplicationDelegate = applicationDelegate;
		}

        protected override void CloseViewIfCan()
		{
			var windows = UIApplication.SharedApplication.Windows;
			var window = windows[0];
			if (mCountClose > 0) 
			{
				mCountClose--;
				var uiNavigationController = window.RootViewController as UINavigationController;
				var viewController = uiNavigationController.ViewControllers.LastOrDefault();
                
                if (viewController is MvxViewController) 
				{
					var mvxViewController = viewController as MvxViewController;
                    mvxViewController.ViewWillDisappearCalled += (object sender, MvvmCross.Platform.Core.MvxValueEventArgs<bool> e) => 
					{
						AppServiceBase.CloseViewIfCan(this);
					};
					var viewModel = mvxViewController.ViewModel as MvxNavigatingObject;
					var close = viewModel.GetType ().GetMethod ("Close", System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance, null, new[] { typeof(IMvxViewModel) }, null);
					close.Invoke (viewModel, new object[] { viewModel });
				}
				else				
				{
					uiNavigationController.PopViewController (true);
					AppServiceBase.CloseViewIfCan(this);
				}
			}
		}

        public override void ShowDialog(string message)
        {
        }

        public override void ShowProgressDialog(string message)
        {			
			DismissProgressDialog();
			var windows = UIApplication.SharedApplication.Windows;
			var window = windows[windows.Length - 1];

			mMTMBProgressHUD = new MTMBProgressHUD (window) {
				LabelText = message,
				RemoveFromSuperViewOnHide = true
			};
			window.AddSubview (mMTMBProgressHUD);
			mMTMBProgressHUD.Show(animated: true);
		}

		public override void DismissProgressDialog()
		{
			var mTMBProgressHUD = mMTMBProgressHUD;
			if (mTMBProgressHUD != null)
			{
				mTMBProgressHUD.Hide (animated: true, delay: 0);
			}
		}

        public override void ShowText(string message)
        {		
			Toast.MakeText(message + "", (int)ToastIOS.ToastDuration.Normal).Show ();
        }
    }
}
