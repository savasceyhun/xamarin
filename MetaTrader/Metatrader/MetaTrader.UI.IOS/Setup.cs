﻿using MetaTrader.Core;
using MetaTrader.UI.IOS.Services;
using MvvmCross.Core.ViewModels;
using MvvmCross.iOS.Platform;
using MvvmCross.iOS.Views.Presenters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using System.Text;

namespace MetaTrader.UI.IOS
{
    public class Setup : MvxIosSetup
    {
        public Setup(MvxApplicationDelegate applicationDelegate, IMvxIosViewPresenter presenter)
            : base(applicationDelegate, presenter)
        {
            CultureInfo cultureInfo = new CultureInfo(System.Threading.Thread.CurrentThread.CurrentCulture.Name, true);
            cultureInfo.NumberFormat = NumberFormatInfo.InvariantInfo;
            System.Threading.Thread.CurrentThread.CurrentCulture = cultureInfo;
            System.Net.ServicePointManager.ServerCertificateValidationCallback += RemoteCertificateValidationCallback;
        }

        protected override IMvxApplication CreateApp()
        {
            AppServices appService = new AppServices((MvxApplicationDelegate)this.ApplicationDelegate);
            return new App(appService);
        }
        


		public bool RemoteCertificateValidationCallback(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certificate,
			System.Security.Cryptography.X509Certificates.X509Chain chain,
			System.Net.Security.SslPolicyErrors sslPolicyErrors)
		{
			if (sender is System.Net.HttpWebRequest)
			{
				System.Net.HttpWebRequest httpWebRequest = sender as System.Net.HttpWebRequest;
				if (httpWebRequest.RequestUri.OriginalString.Contains(AppConstants.ROOT_API_META_TRADER_TURKDEX))
				{
					return true;
				}
			}
			return false;
		}

    }
}
