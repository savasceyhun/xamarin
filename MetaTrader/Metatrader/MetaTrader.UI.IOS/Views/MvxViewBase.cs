﻿using MvvmCross.iOS.Views;
using System;
using System.Collections.Generic;
using System.Text;
using UIKit;

namespace MetaTrader.UI.IOS
{
    public abstract class MvxViewBase : MvxViewController
    {
		public MvxViewBase(string nibName, Foundation.NSBundle bundle): base(nibName, bundle)
		{
		}

		public MvxViewBase(): base()
		{
		}

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            if (this.NavigationController != null)
            {
                this.NavigationController.SetNavigationBarHidden(true, true);
            }
        }

		public int GetVersion(string systemVersion)
		{
			int version = 0;
			int.TryParse(UIDevice.CurrentDevice.SystemVersion.Split('.')[0], out version);
			return version;
		}
    }
}
