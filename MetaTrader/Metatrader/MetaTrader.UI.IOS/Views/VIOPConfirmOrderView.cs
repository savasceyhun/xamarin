﻿using System;
using System.Timers;
using MetaTrader.Core.ViewModels;
using MvvmCross.Binding.BindingContext;
using UIKit;

namespace MetaTrader.UI.IOS
{
	public partial class VIOPConfirmOrderView : MvxViewBase
	{
		private Timer mTimer;
		private int mPeriod = 1 * 1 * 1000;
		private bool mIsRunning = false;

		public VIOPConfirmOrderView() : base ("VIOPConfirmOrderView", null)
		{
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			// Perform any additional setup after loading the view, typically from a nib.

			this.CreateBinding(ButtonBack).For("Tap").To((VIOPConfirmOrderViewModel vm) => vm.BackCommand).Apply();
			this.CreateBinding(ButtonConfirm).For("Tap").To((VIOPConfirmOrderViewModel vm) => vm.PlaceOrderCommand).Apply();
			this.CreateBinding(ButtonCancel).For("Tap").To((VIOPConfirmOrderViewModel vm) => vm.CancelCommand).Apply();

			this.CreateBinding(LabelFiyat).For((v) => v.Text).To((VIOPConfirmOrderViewModel vm) => vm.Price).Apply();
			this.CreateBinding(LabelMiktar).For((v) => v.Text).To((VIOPConfirmOrderViewModel vm) => vm.Amount).Apply();
			this.CreateBinding(LabelSuresi).For((v) => v.Text).To((VIOPConfirmOrderViewModel vm) => vm.Sure).Apply();
			this.CreateBinding(LabelSozlesme).For((v) => v.Text).To((VIOPConfirmOrderViewModel vm) => vm.VIOP.Code).Apply();
			this.CreateBinding(LabelFiyatSekli).For((v) => v.Text).To((VIOPConfirmOrderViewModel vm) => vm.FiyatTipi).Apply();
		}

		public override void DidReceiveMemoryWarning()
		{
			base.DidReceiveMemoryWarning();
			// Release any cached data, images, etc that aren't in use.
		}


		public override void ViewDidDisappear(bool animated)
		{
			base.ViewDidDisappear(animated);
			try
			{
				mTimer.Stop();
				mTimer.Dispose();
			}
			catch (Exception)
			{
			}
			mTimer = null;
		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);
			try
			{
				if (mTimer == null)
				{
					mTimer = new Timer();
					mTimer.Interval = mPeriod;
					mTimer.Elapsed += (sender, e) =>
					{
						if (!mIsRunning)
						{
							mIsRunning = true;
							(this.ViewModel as VIOPConfirmOrderViewModel).LoadPrice();
							mIsRunning = false;
						}
					};
				}

				mTimer.Start();

			}
			catch (Exception)
			{
			}
		}
	}
}


