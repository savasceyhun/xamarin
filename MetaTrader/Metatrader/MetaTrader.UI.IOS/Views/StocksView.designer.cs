// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace MetaTrader.UI.IOS
{
    [Register ("StocksView")]
    partial class StocksView
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        MetaTrader.UI.IOS.UIButtonDefault ButtonAccount { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton ButtonBack { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        MetaTrader.UI.IOS.UIButtonDefault ButtonOrders { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        MetaTrader.UI.IOS.UIButtonDefault ButtonPositions { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        MetaTrader.UI.IOS.UIButtonDefault ButtonQuotes { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UISearchBar SearchBarStock { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableView TableStocks { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (ButtonAccount != null) {
                ButtonAccount.Dispose ();
                ButtonAccount = null;
            }

            if (ButtonBack != null) {
                ButtonBack.Dispose ();
                ButtonBack = null;
            }

            if (ButtonOrders != null) {
                ButtonOrders.Dispose ();
                ButtonOrders = null;
            }

            if (ButtonPositions != null) {
                ButtonPositions.Dispose ();
                ButtonPositions = null;
            }

            if (ButtonQuotes != null) {
                ButtonQuotes.Dispose ();
                ButtonQuotes = null;
            }

            if (SearchBarStock != null) {
                SearchBarStock.Dispose ();
                SearchBarStock = null;
            }

            if (TableStocks != null) {
                TableStocks.Dispose ();
                TableStocks = null;
            }
        }
    }
}