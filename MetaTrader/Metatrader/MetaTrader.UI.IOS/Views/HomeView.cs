using MvvmCross.iOS.Views;
using System;

using UIKit;
using MvvmCross.Binding.BindingContext;
using MetaTrader.Core.ViewModels;

namespace MetaTrader.UI.IOS.Views
{
    public partial class HomeView : MvxViewBase
    {
        public HomeView() : base("HomeView", null)
        {
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();

            // Release any cached data, images, etc that aren't in use.
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

			this.CreateBinding(ButtonBack).For("Tap").To((StockConfirmOrderViewModel vm) => vm.BackCommand).Apply();
			this.CreateBinding(BtnStocks).For("Tap").To((HomeViewModel vm) => vm.StocksCommand).Apply();
			this.CreateBinding(BtnAccount).For("Tap").To((HomeViewModel vm) => vm.AccountCommand).Apply();
			this.CreateBinding(BtnDerivatives).For("Tap").To((HomeViewModel vm) => vm.DerivativesCommand).Apply();
			this.CreateBinding(BtnMoneyTranfers).To((HomeViewModel vm) => vm.MoneyTransfersCommand).Apply();

        }

    }
}