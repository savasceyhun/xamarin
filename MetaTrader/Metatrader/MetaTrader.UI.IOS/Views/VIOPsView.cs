﻿using System;
using System.Collections.Generic;
using MetaTrader.Core.ViewModels;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.iOS.Views;
using ObjCRuntime;
using UIKit;

namespace MetaTrader.UI.IOS
{
	public partial class VIOPsView : MvxViewBase
	{
		public VIOPsView() : base("VIOPsView", null)
		{
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			// Perform any additional setup after loading the view, typically from a nib.

			this.CreateBinding(ButtonBack).For("Tap").To((VIOPsViewModel vm) => vm.BackCommand).Apply();

			var source = new VIOPTableSource(TableVIOPs);
			this.AddBindings(new Dictionary<object, string>
				{
					{source, "ItemsSource VIOPsList"}
				});

			TableVIOPs.Source = source;
			TableVIOPs.ReloadData();

			this.CreateBinding(ButtonBack).For("Tap").To((StockConfirmOrderViewModel vm) => vm.BackCommand).Apply();
			this.CreateBinding(SearchBarVIOP).For((v) => v.Text).To((StocksViewModel vm) => vm.SearchText).Apply();

			SearchBarVIOP.EnablesReturnKeyAutomatically = false;
			SearchBarVIOP.SearchButtonClicked += (object sender, EventArgs e) =>
			{
				SearchBarVIOP.ResignFirstResponder();
			};

			var protocol = Runtime.GetProtocol("UITextInputTraits");
			foreach (var subview in SearchBarVIOP.Subviews)
			{
				foreach (var view in subview.Subviews)
				{
					if (view.ConformsToProtocol(protocol))
					{
						UITextField textField = (UITextField)view;
						textField.KeyboardAppearance = UIKeyboardAppearance.Alert;
						textField.ReturnKeyType = UIReturnKeyType.Done;
						textField.EnablesReturnKeyAutomatically = false;
					}
				}
			}

		}

		public override void DidReceiveMemoryWarning()
		{
			base.DidReceiveMemoryWarning();
			// Release any cached data, images, etc that aren't in use.
		}


		public class VIOPTableSource : MvxSimpleTableViewSource
		{
			public VIOPTableSource(UITableView tableView)
				: base(tableView, "VIOPCell", "VIOPCell")
			{
			}
		}
	}
}


