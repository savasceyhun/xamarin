﻿using System;

using UIKit;
using MvvmCross.Binding.BindingContext;
using MetaTrader.Core.ViewModels;
using System.Timers;

namespace MetaTrader.UI.IOS
{
	public partial class ConfirmOrderView : MvxViewBase
	{
		private Timer mTimer;
		private int mPeriod = 1 * 1 * 1000;
		private bool mIsRunning = false;

		public ConfirmOrderView () : base ("ConfirmOrderView", null)
		{
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad();
			// Perform any additional setup after loading the view, typically from a nib.

			this.CreateBinding(ButtonBack).For("Tap").To((StockConfirmOrderViewModel vm) => vm.BackCommand).Apply();
			this.CreateBinding(ButtonConfirm).For("Tap").To((StockConfirmOrderViewModel vm) => vm.PlaceOrderCommand).Apply();
			this.CreateBinding(ButtonCancel).For("Tap").To((StockConfirmOrderViewModel vm) => vm.CancelCommand).Apply();

			this.CreateBinding(LabelFiyat).For((v) => v.Text).To((StockConfirmOrderViewModel vm) => vm.Price).Apply();
			this.CreateBinding(LabelMiktar).For((v) => v.Text).To((StockConfirmOrderViewModel vm) => vm.Amount).Apply();
			this.CreateBinding(LabelSuresi).For((v) => v.Text).To((StockConfirmOrderViewModel vm) => vm.OrderMethod).Apply();
			this.CreateBinding(LabelSozlesme).For((v) => v.Text).To((StockConfirmOrderViewModel vm) => vm.Stock.Name).Apply();
			this.CreateBinding(LabelFiyatSekli).For((v) => v.Text).To((StockConfirmOrderViewModel vm) => vm.OrderType).Apply();
		}

		public override void DidReceiveMemoryWarning ()
		{
			base.DidReceiveMemoryWarning ();
			// Release any cached data, images, etc that aren't in use.
		}


		public override void ViewDidDisappear (bool animated)
		{
			base.ViewDidDisappear (animated);
			try {
				mTimer.Stop ();
				mTimer.Dispose();
			} catch (Exception) {
			}
			mTimer = null;
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			try {
				if(mTimer == null){
					mTimer = new Timer ();
					mTimer.Interval = mPeriod;
					mTimer.Elapsed += (sender, e) => 
					{
						if (!mIsRunning)
						{
							mIsRunning = true;
							(this.ViewModel as StockConfirmOrderViewModel).LoadPrice();
							mIsRunning = false;
						}
					};
				}

				mTimer.Start ();

			} catch (Exception) {
			}
		}
	}
}


