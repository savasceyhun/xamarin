﻿using System;

using UIKit;
using MvvmCross.Binding.BindingContext;
using MetaTrader.Core.ViewModels;
using MvvmCross.Binding.iOS.Views;
using OxyPlot.Xamarin.iOS;
using CoreGraphics;
using System.Timers;

namespace MetaTrader.UI.IOS
{
	public partial class StockTradeView : MvxViewBase
	{
		private Timer mTimer;
		private int mPeriod = 1 * 1 * 1000;
		private bool mIsRunning = false;

		private PlotView mPlotView;

		public StockTradeView () : base ("StockTradeView", null)
		{
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			// Perform any additional setup after loading the view, typically from a nib.

			var picker = new UIPickerView();
			var pickerViewModel = new MvxPickerViewModel(picker);
			picker.Model = pickerViewModel;
			picker.ShowSelectionIndicator = true;
			TextViewPrice.InputView = picker;

			this.mPlotView = new PlotView 
			{
				Frame = new CoreGraphics.CGRect(new CGPoint(0,0), ViewChart.Frame.Size)
			};
			this.ViewChart.AddSubview (mPlotView);
			this.mPlotView.TranslatesAutoresizingMaskIntoConstraints = false;
			ViewChart.AddConstraint(NSLayoutConstraint.Create(mPlotView, NSLayoutAttribute.Width, NSLayoutRelation.Equal, ViewChart, NSLayoutAttribute.Width, 1, 0));
			ViewChart.AddConstraint(NSLayoutConstraint.Create(mPlotView, NSLayoutAttribute.Height, NSLayoutRelation.Equal, ViewChart, NSLayoutAttribute.Height, 1, 0));
			ViewChart.AddConstraint(NSLayoutConstraint.Create(mPlotView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, ViewChart, NSLayoutAttribute.Top, 1, 0));
			ViewChart.AddConstraint(NSLayoutConstraint.Create(mPlotView, NSLayoutAttribute.Leading, NSLayoutRelation.Equal, ViewChart, NSLayoutAttribute.Leading, 1, 0));


			var g = new UITapGestureRecognizer(() => TextViewPrice.ResignFirstResponder());
			this.View.AddGestureRecognizer(g);
			g = new UITapGestureRecognizer(() => TextViewPrice.ResignFirstResponder());
			this.ViewChart.AddGestureRecognizer(g);
			g = new UITapGestureRecognizer(() => TextViewPrice.ResignFirstResponder());
			this.ViewSpecs.AddGestureRecognizer(g);
		

			this.CreateBinding(LabelStockName).For((v) => v.Text).To((StockTradeViewModel vm) => vm.Stock.Name).Apply();
			this.CreateBinding(pickerViewModel).For(p => p.ItemsSource).To((StockTradeViewModel vm) => vm.TickValues).Apply();
			this.CreateBinding(pickerViewModel).For(p => p.SelectedItem).To((StockTradeViewModel vm) => vm.TickValue).Apply();
			this.CreateBinding(TextViewPrice).For((v) => v.Enabled).To((StockTradeViewModel vm) => vm.PriceEnabled).Apply();

			this.CreateBinding(TextViewPrice).For((v) => v.Text).To((StockTradeViewModel vm) => vm.TickValue).Apply();
			this.CreateBinding(TextViewAmount).For((v) => v.Text).To((StockTradeViewModel vm) => vm.Amount).Apply();
			this.CreateBinding(mPlotView).For((v) => v.Model).To((StockTradeViewModel vm) => vm.PlotModel).Apply();
			this.CreateBinding(ButtonBack).For("Tap").To((StockConfirmOrderViewModel vm) => vm.BackCommand).Apply();
			this.CreateBinding(ButtonBuy).For("Tap").To((StockTradeViewModel vm) => vm.BuyCommand).Apply();
			this.CreateBinding(ButtonSell).For("Tap").To((StockTradeViewModel vm) => vm.SellCommand).Apply();


			this.CreateBinding(RadioButtonLimit).For((v) => v.Checked).To((StockTradeViewModel vm) => vm.LimitChecked).Apply();
			this.CreateBinding(RadioButtonDengeleyici).For((v) => v.Checked).To((StockTradeViewModel vm) => vm.DengeleyiciChecked).Apply();
			this.CreateBinding(RadioButtonGunluk).For((v) => v.Checked).To((StockTradeViewModel vm) => vm.GunlukChecked).Apply();
			this.CreateBinding(RadioButtonKIE).For((v) => v.Checked).To((StockTradeViewModel vm) => vm.KIEChecked).Apply();
			this.CreateBinding(RadioButtonPiyasa).For((v) => v.Checked).To((StockTradeViewModel vm) => vm.PiyasaChecked).Apply();
			this.CreateBinding(RadioButtonPiyasadanLimite).For((v) => v.Checked).To((StockTradeViewModel vm) => vm.PiyasadanLimiteChecked).Apply();

			this.CreateBinding(RadioButtonGunluk).For((v) => v.Enabled).To((StockTradeViewModel vm) => vm.OrderMethodEnabled).Apply();
			this.CreateBinding(RadioButtonKIE).For((v) => v.Enabled).To((StockTradeViewModel vm) => vm.OrderMethodEnabled).Apply();

			this.CreateBinding(ViewChart).For((v) => v.Hidden).To((StockTradeViewModel vm) => vm.IsShowedSpecs).Apply();
			this.CreateBinding(ViewSpecs).For((v) => v.Hidden).To((StockTradeViewModel vm) => vm.IsShowedChart).Apply();
			this.CreateBinding(ButtonTabChart).For((v) => v.Checked).To((StockTradeViewModel vm) => vm.IsShowedChart).Apply();
			this.CreateBinding(ButtonTabSpecs).For((v) => v.Checked).To((StockTradeViewModel vm) => vm.IsShowedSpecs).Apply();


			this.CreateBinding(LabelDescription).For((v) => v.Text).To((StockTradeViewModel vm) => vm.Stock.Name).Apply();
			this.CreateBinding(LabelTickSize).For((v) => v.Text).To((StockTradeViewModel vm) => vm.Stock.TickValue).Apply();
			this.CreateBinding(LabelBasePrice).For((v) => v.Text).To((StockTradeViewModel vm) => vm.Stock.BasePrice).Apply();
			this.CreateBinding(LabelDate).For((v) => v.Text).To((StockTradeViewModel vm) => vm.Stock.Date).Apply();
			this.CreateBinding(LabelCeilingPrice).For((v) => v.Text).To((StockTradeViewModel vm) => vm.Stock.CeilingPrice).Apply();

			this.CreateBinding(ButtonBuy).For((v) => v.TitleText).To((StockTradeViewModel vm) => vm.BuyMessage).Apply();
			this.CreateBinding(ButtonSell).For((v) => v.TitleText).To((StockTradeViewModel vm) => vm.SellMessage).Apply();
		}

		public override void ViewDidDisappear (bool animated)
		{
			base.ViewDidDisappear (animated);
			try {
				mTimer.Stop ();
				mTimer.Dispose();
			} catch (Exception) {
			}
			mTimer = null;
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			try {
				if(mTimer == null){
					mTimer = new Timer ();
					mTimer.Interval = mPeriod;
					mTimer.Elapsed += (sender, e) => 
					{
						if (!mIsRunning)
						{
							mIsRunning = true;
							(this.ViewModel as StockTradeViewModel).LoadStockWithoutProgressDialog();
							mIsRunning = false;
						}
					};
				}

				mTimer.Start ();

			} catch (Exception) {
			}
		}

		public override void DidReceiveMemoryWarning ()
		{
			base.DidReceiveMemoryWarning ();
			// Release any cached data, images, etc that aren't in use.
		}

		// Invalidate the plot view when the orientation of the device changes
		public override void DidRotate (UIInterfaceOrientation fromInterfaceOrientation)
		{
			base.DidRotate (fromInterfaceOrientation);
			this.mPlotView.InvalidatePlot(false);
		}	
	}
}


