// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace MetaTrader.UI.IOS
{
    [Register ("VIOPTradeView")]
    partial class VIOPTradeView
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton ButtonBack { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        MetaTrader.UI.IOS.UIButtonGreen ButtonBuy { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        MetaTrader.UI.IOS.UIButtonRed ButtonSell { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        MetaTrader.UI.IOS.UITabButton ButtonTabChart { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        MetaTrader.UI.IOS.UITabButton ButtonTabSpecs { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelAlisDerinligi { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelAlisFiyat { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelCode { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelSatisDerinligi { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelSatisFiyat { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelStockName { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelTabanFiyat { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelTavanFiyat { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        MetaTrader.UI.IOS.RadioButton RadioButtonEnIyiFiyat { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        MetaTrader.UI.IOS.RadioButton RadioButtonGerceklesmezseIptalEt { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        MetaTrader.UI.IOS.RadioButton RadioButtonGunluk { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        MetaTrader.UI.IOS.RadioButton RadioButtonIptaleKadarGecerli { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        MetaTrader.UI.IOS.RadioButton RadioButtonKalaniIptalEt { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        MetaTrader.UI.IOS.RadioButton RadioButtonKalaniPasifeYaz { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        MetaTrader.UI.IOS.RadioButton RadioButtonLimitli { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        MetaTrader.UI.IOS.RadioButton RadioButtonPiyasa { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        MetaTrader.UI.IOS.RadioButton RadioButtonTarihli { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        MetaTrader.UI.IOS.UITextViewDefault TextViewAmount { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        MetaTrader.UI.IOS.UITextViewDefault TextViewPrice { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        MetaTrader.UI.IOS.UITextViewDefault TextViewTarih { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView ViewChart { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        MetaTrader.UI.IOS.UIViewRounded ViewEmirTipi { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        MetaTrader.UI.IOS.UIViewRounded ViewFiyatTipi { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView ViewSpecs { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        MetaTrader.UI.IOS.UIViewRounded ViewSure { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (ButtonBack != null) {
                ButtonBack.Dispose ();
                ButtonBack = null;
            }

            if (ButtonBuy != null) {
                ButtonBuy.Dispose ();
                ButtonBuy = null;
            }

            if (ButtonSell != null) {
                ButtonSell.Dispose ();
                ButtonSell = null;
            }

            if (ButtonTabChart != null) {
                ButtonTabChart.Dispose ();
                ButtonTabChart = null;
            }

            if (ButtonTabSpecs != null) {
                ButtonTabSpecs.Dispose ();
                ButtonTabSpecs = null;
            }

            if (LabelAlisDerinligi != null) {
                LabelAlisDerinligi.Dispose ();
                LabelAlisDerinligi = null;
            }

            if (LabelAlisFiyat != null) {
                LabelAlisFiyat.Dispose ();
                LabelAlisFiyat = null;
            }

            if (LabelCode != null) {
                LabelCode.Dispose ();
                LabelCode = null;
            }

            if (LabelSatisDerinligi != null) {
                LabelSatisDerinligi.Dispose ();
                LabelSatisDerinligi = null;
            }

            if (LabelSatisFiyat != null) {
                LabelSatisFiyat.Dispose ();
                LabelSatisFiyat = null;
            }

            if (LabelStockName != null) {
                LabelStockName.Dispose ();
                LabelStockName = null;
            }

            if (LabelTabanFiyat != null) {
                LabelTabanFiyat.Dispose ();
                LabelTabanFiyat = null;
            }

            if (LabelTavanFiyat != null) {
                LabelTavanFiyat.Dispose ();
                LabelTavanFiyat = null;
            }

            if (RadioButtonEnIyiFiyat != null) {
                RadioButtonEnIyiFiyat.Dispose ();
                RadioButtonEnIyiFiyat = null;
            }

            if (RadioButtonGerceklesmezseIptalEt != null) {
                RadioButtonGerceklesmezseIptalEt.Dispose ();
                RadioButtonGerceklesmezseIptalEt = null;
            }

            if (RadioButtonGunluk != null) {
                RadioButtonGunluk.Dispose ();
                RadioButtonGunluk = null;
            }

            if (RadioButtonIptaleKadarGecerli != null) {
                RadioButtonIptaleKadarGecerli.Dispose ();
                RadioButtonIptaleKadarGecerli = null;
            }

            if (RadioButtonKalaniIptalEt != null) {
                RadioButtonKalaniIptalEt.Dispose ();
                RadioButtonKalaniIptalEt = null;
            }

            if (RadioButtonKalaniPasifeYaz != null) {
                RadioButtonKalaniPasifeYaz.Dispose ();
                RadioButtonKalaniPasifeYaz = null;
            }

            if (RadioButtonLimitli != null) {
                RadioButtonLimitli.Dispose ();
                RadioButtonLimitli = null;
            }

            if (RadioButtonPiyasa != null) {
                RadioButtonPiyasa.Dispose ();
                RadioButtonPiyasa = null;
            }

            if (RadioButtonTarihli != null) {
                RadioButtonTarihli.Dispose ();
                RadioButtonTarihli = null;
            }

            if (TextViewAmount != null) {
                TextViewAmount.Dispose ();
                TextViewAmount = null;
            }

            if (TextViewPrice != null) {
                TextViewPrice.Dispose ();
                TextViewPrice = null;
            }

            if (TextViewTarih != null) {
                TextViewTarih.Dispose ();
                TextViewTarih = null;
            }

            if (ViewChart != null) {
                ViewChart.Dispose ();
                ViewChart = null;
            }

            if (ViewEmirTipi != null) {
                ViewEmirTipi.Dispose ();
                ViewEmirTipi = null;
            }

            if (ViewFiyatTipi != null) {
                ViewFiyatTipi.Dispose ();
                ViewFiyatTipi = null;
            }

            if (ViewSpecs != null) {
                ViewSpecs.Dispose ();
                ViewSpecs = null;
            }

            if (ViewSure != null) {
                ViewSure.Dispose ();
                ViewSure = null;
            }
        }
    }
}