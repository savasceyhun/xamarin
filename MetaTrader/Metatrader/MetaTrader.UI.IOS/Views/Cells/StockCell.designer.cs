// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace MetaTrader.UI.IOS
{
    [Register ("StockCell")]
    partial class StockCell
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lbHighestPrice { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lbLastPrice { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lbLowestPrice { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lbName { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        MetaTrader.UI.IOS.UIViewClickDefault ViewRoot { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (lbHighestPrice != null) {
                lbHighestPrice.Dispose ();
                lbHighestPrice = null;
            }

            if (lbLastPrice != null) {
                lbLastPrice.Dispose ();
                lbLastPrice = null;
            }

            if (lbLowestPrice != null) {
                lbLowestPrice.Dispose ();
                lbLowestPrice = null;
            }

            if (lbName != null) {
                lbName.Dispose ();
                lbName = null;
            }

            if (ViewRoot != null) {
                ViewRoot.Dispose ();
                ViewRoot = null;
            }
        }
    }
}