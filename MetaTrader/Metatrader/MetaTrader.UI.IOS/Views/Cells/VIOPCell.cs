﻿using System;

using Foundation;
using MetaTrader.Core;
using MetaTrader.Core.ViewModels;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.iOS.Views;
using UIKit;

namespace MetaTrader.UI.IOS
{
	public partial class VIOPCell : MvxTableViewCell
	{
		public string Sozlesme
		{
			get { return LabelSozlesme.Text; }
			set { LabelSozlesme.Text = value; }
		}

		public string AlisFiyat
		{
			get { return LabelAlisFiyat.Text; }
			set { LabelAlisFiyat.Text = value; }
		}

		public string SatisFiyat
		{
			get { return LabelSatisFiyat.Text; }
			set { LabelSatisFiyat.Text = value; }
		}

		public string AlisDerinlik
		{
			get { return LabelAlisDerinlik.Text; }
			set { LabelAlisDerinlik.Text = value; }
		}

		public string SatisDerinlik
		{
			get { return LabelSatisDerinlik.Text; }
			set { LabelSatisDerinlik.Text = value; }
		}

		public VIOPCell()
		{
			InitialiseBindings();
		}

		public VIOPCell(IntPtr handle) : base (handle)
		{
			InitialiseBindings();
			this.SelectionStyle = UITableViewCellSelectionStyle.None;
		}


		private void InitialiseBindings()
		{
			// this is equivalent to:
			this.DelayBind(() =>
				{
					this.CreateBinding().For((cell) => cell.Sozlesme).To((ItemViewModel<VIOP> VIOPItemViewModel) =>
				                                                         VIOPItemViewModel.Model.Name
					).Apply();
				this.CreateBinding().For((cell) => cell.AlisFiyat).To((ItemViewModel<VIOP> VIOPItemViewModel) =>
						VIOPItemViewModel.Model.AlisFiyat
					).Apply();
				this.CreateBinding().For((cell) => cell.AlisDerinlik).To((ItemViewModel<VIOP> VIOPItemViewModel) =>
						VIOPItemViewModel.Model.AlisDerinlik
					).Apply();
				this.CreateBinding().For((cell) => cell.SatisFiyat).To((ItemViewModel<VIOP> VIOPItemViewModel) =>
						VIOPItemViewModel.Model.SatisFiyat
					).Apply();
					this.CreateBinding().For((cell) => cell.SatisDerinlik).To((ItemViewModel<VIOP> VIOPItemViewModel) =>
						VIOPItemViewModel.Model.SatisDerinlik
					).Apply();

					this.CreateBinding(this).For("Tap").To((ItemViewModel<VIOP> VIOPItemViewModel) =>
						VIOPItemViewModel.ItemClickCommand
					).Apply();
				});
		}

		public static float GetCellHeight()
		{
			return 60f;
		}
	}
}
