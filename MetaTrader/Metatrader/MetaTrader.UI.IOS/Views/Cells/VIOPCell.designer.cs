// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace MetaTrader.UI.IOS
{
    [Register ("VIOPCell")]
    partial class VIOPCell
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelAlisDerinlik { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelAlisFiyat { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelSatisDerinlik { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelSatisFiyat { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelSozlesme { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        MetaTrader.UI.IOS.UIViewClickDefault ViewRoot { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (LabelAlisDerinlik != null) {
                LabelAlisDerinlik.Dispose ();
                LabelAlisDerinlik = null;
            }

            if (LabelAlisFiyat != null) {
                LabelAlisFiyat.Dispose ();
                LabelAlisFiyat = null;
            }

            if (LabelSatisDerinlik != null) {
                LabelSatisDerinlik.Dispose ();
                LabelSatisDerinlik = null;
            }

            if (LabelSatisFiyat != null) {
                LabelSatisFiyat.Dispose ();
                LabelSatisFiyat = null;
            }

            if (LabelSozlesme != null) {
                LabelSozlesme.Dispose ();
                LabelSozlesme = null;
            }

            if (ViewRoot != null) {
                ViewRoot.Dispose ();
                ViewRoot = null;
            }
        }
    }
}