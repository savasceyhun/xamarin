﻿using System;

using Foundation;
using UIKit;
using MvvmCross.Binding.iOS.Views;
using MvvmCross.Binding.BindingContext;
using MetaTrader.Core.Models;
using MetaTrader.Core.ViewModels;

namespace MetaTrader.UI.IOS
{
	public partial class StockCell : MvxTableViewCell
	{
		public string Name
		{
			get { return lbName.Text; }
			set { lbName.Text = value; }
		}

		public string LastPrice
		{
			get { return lbLastPrice.Text; }
			set { lbLastPrice.Text = value; }
		}

		public string LowestPrice
		{
			get { return lbLowestPrice.Text; }
			set { lbLowestPrice.Text = value; }
		}

		public string HighestPrice
		{
			get { return lbHighestPrice.Text; }
			set { lbHighestPrice.Text = value; }
		}

		public StockCell ()
		{
			InitialiseBindings();
		}

		public StockCell (IntPtr handle) : base (handle)
		{
			InitialiseBindings();
			this.SelectionStyle = UITableViewCellSelectionStyle.None;
		}


		private void InitialiseBindings()
		{
			// this is equivalent to:
			this.DelayBind(() =>
				{
					this.CreateBinding().For((cell) => cell.Name).To((ItemViewModel<Stock> stockItemViewModel) =>
						stockItemViewModel.Model.Name
					).Apply();
					this.CreateBinding().For((cell) => cell.LastPrice).To((ItemViewModel<Stock> stockItemViewModel) =>
						stockItemViewModel.Model.LastPrice
					).Apply();
					this.CreateBinding().For((cell) => cell.LowestPrice).To((ItemViewModel<Stock> stockItemViewModel) =>
						stockItemViewModel.Model.LowestPrice
					).Apply();
					this.CreateBinding().For((cell) => cell.HighestPrice).To((ItemViewModel<Stock> stockItemViewModel) =>
						stockItemViewModel.Model.HighestPrice
					).Apply();

					this.CreateBinding(this).For("Tap").To((ItemViewModel<Stock> stockItemViewModel) =>
						stockItemViewModel.ItemClickCommand
					).Apply();				
				});			
		}

		public static float GetCellHeight()
		{
			return 120f;
		}
	}
}
