// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace MetaTrader.UI.IOS
{
    [Register ("ConfirmOrderView")]
    partial class ConfirmOrderView
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton ButtonBack { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        MetaTrader.UI.IOS.UIButtonRed ButtonCancel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        MetaTrader.UI.IOS.UIButtonDefault ButtonConfirm { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelFiyat { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelFiyatSekli { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelMiktar { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelSozlesme { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelSuresi { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (ButtonBack != null) {
                ButtonBack.Dispose ();
                ButtonBack = null;
            }

            if (ButtonCancel != null) {
                ButtonCancel.Dispose ();
                ButtonCancel = null;
            }

            if (ButtonConfirm != null) {
                ButtonConfirm.Dispose ();
                ButtonConfirm = null;
            }

            if (LabelFiyat != null) {
                LabelFiyat.Dispose ();
                LabelFiyat = null;
            }

            if (LabelFiyatSekli != null) {
                LabelFiyatSekli.Dispose ();
                LabelFiyatSekli = null;
            }

            if (LabelMiktar != null) {
                LabelMiktar.Dispose ();
                LabelMiktar = null;
            }

            if (LabelSozlesme != null) {
                LabelSozlesme.Dispose ();
                LabelSozlesme = null;
            }

            if (LabelSuresi != null) {
                LabelSuresi.Dispose ();
                LabelSuresi = null;
            }
        }
    }
}