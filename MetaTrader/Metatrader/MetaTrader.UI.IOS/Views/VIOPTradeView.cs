﻿using System;
using System.Timers;
using CoreGraphics;
using Foundation;
using MetaTrader.Core.ViewModels;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.iOS.Views;
using OxyPlot.Xamarin.iOS;
using UIKit;

namespace MetaTrader.UI.IOS
{
	public partial class VIOPTradeView : MvxViewBase
	{
		private Timer mTimer;
		private int mPeriod = 1 * 1 * 1000;
		private bool mIsRunning = false;

		private PlotView mPlotView;

		public VIOPTradeView() : base ("VIOPTradeView", null)
		{
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			// Perform any additional setup after loading the view, typically from a nib.

			var picker = new UIPickerView();
			var pickerViewModel = new MvxPickerViewModel(picker);
			picker.Model = pickerViewModel;
			picker.ShowSelectionIndicator = true;
			TextViewPrice.InputView = picker;

			NSDateFormatter dateFormatter = new NSDateFormatter();
			dateFormatter.DateFormat = "dd/MM/yyyy";

			UIDatePicker datePicker = new UIDatePicker();
			NSLocale uk = new NSLocale("en_GB");
			NSCalendar cal = NSCalendar.CurrentCalendar;
			cal.Locale = uk;
			datePicker.Calendar = cal;
			datePicker.Mode = UIDatePickerMode.Date;
			TextViewTarih.InputView = datePicker;
			datePicker.ValueChanged += (sender, e) =>
			{
				this.InvokeOnMainThread(() =>
				{
					TextViewTarih.Text = dateFormatter.ToString(datePicker.Date);
				});
			};
			//datePicker.Date = new NSDate();

			this.mPlotView = new PlotView
			{
				Frame = new CoreGraphics.CGRect(new CGPoint(0, 0), ViewChart.Frame.Size)
			};
			this.ViewChart.AddSubview(mPlotView);
			this.mPlotView.TranslatesAutoresizingMaskIntoConstraints = false;
			ViewChart.AddConstraint(NSLayoutConstraint.Create(mPlotView, NSLayoutAttribute.Width, NSLayoutRelation.Equal, ViewChart, NSLayoutAttribute.Width, 1, 0));
			ViewChart.AddConstraint(NSLayoutConstraint.Create(mPlotView, NSLayoutAttribute.Height, NSLayoutRelation.Equal, ViewChart, NSLayoutAttribute.Height, 1, 0));
			ViewChart.AddConstraint(NSLayoutConstraint.Create(mPlotView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, ViewChart, NSLayoutAttribute.Top, 1, 0));
			ViewChart.AddConstraint(NSLayoutConstraint.Create(mPlotView, NSLayoutAttribute.Leading, NSLayoutRelation.Equal, ViewChart, NSLayoutAttribute.Leading, 1, 0));


			var g = new UITapGestureRecognizer(() =>
			{
				TextViewPrice.ResignFirstResponder();
				TextViewTarih.ResignFirstResponder();
			});
			this.View.AddGestureRecognizer(g);
			g = new UITapGestureRecognizer(() =>
			{
				TextViewPrice.ResignFirstResponder();
				TextViewTarih.ResignFirstResponder();
			});
			this.ViewChart.AddGestureRecognizer(g);
			g = new UITapGestureRecognizer(() =>
			{
				TextViewPrice.ResignFirstResponder();
				TextViewTarih.ResignFirstResponder();
			});
			this.ViewSpecs.AddGestureRecognizer(g);


			this.CreateBinding(LabelStockName).For((v) => v.Text).To((VIOPTradeViewModel vm) => vm.VIOP.Name).Apply();
			this.CreateBinding(LabelCode).For((v) => v.Text).To((VIOPTradeViewModel vm) => vm.VIOP.Code).Apply();

			this.CreateBinding(pickerViewModel).For(p => p.ItemsSource).To((VIOPTradeViewModel vm) => vm.TickValues).Apply();
			this.CreateBinding(pickerViewModel).For(p => p.SelectedItem).To((VIOPTradeViewModel vm) => vm.TickValue).Apply();
			this.CreateBinding(TextViewPrice).For((v) => v.Enabled).To((VIOPTradeViewModel vm) => vm.PriceEnabled).Apply();
			this.CreateBinding(TextViewTarih).For((v) => v.Enabled).To((VIOPTradeViewModel vm) => vm.TarihEnabled).Apply();

			this.CreateBinding(TextViewTarih).For((v) => v.Text).To((VIOPTradeViewModel vm) => vm.Tarih).Apply();
			this.CreateBinding(TextViewPrice).For((v) => v.Text).To((VIOPTradeViewModel vm) => vm.TickValue).Apply();
			this.CreateBinding(TextViewAmount).For((v) => v.Text).To((VIOPTradeViewModel vm) => vm.Amount).Apply();
			this.CreateBinding(mPlotView).For((v) => v.Model).To((VIOPTradeViewModel vm) => vm.PlotModel).Apply();
			this.CreateBinding(ButtonBack).For("Tap").To((VIOPTradeViewModel vm) => vm.BackCommand).Apply();
			this.CreateBinding(ButtonBuy).For("Tap").To((VIOPTradeViewModel vm) => vm.BuyCommand).Apply();
			this.CreateBinding(ButtonSell).For("Tap").To((VIOPTradeViewModel vm) => vm.SellCommand).Apply();


			this.CreateBinding(RadioButtonEnIyiFiyat).For((v) => v.Checked).To((VIOPTradeViewModel vm) => vm.EnIyiFiyatChecked).Apply();
			this.CreateBinding(RadioButtonGerceklesmezseIptalEt).For((v) => v.Checked).To((VIOPTradeViewModel vm) => vm.GerceklesmezseIptalEtChecked).Apply();
			this.CreateBinding(RadioButtonGunluk).For((v) => v.Checked).To((VIOPTradeViewModel vm) => vm.GunlukChecked).Apply();
			this.CreateBinding(RadioButtonIptaleKadarGecerli).For((v) => v.Checked).To((VIOPTradeViewModel vm) => vm.IptaleKadarGecerliChecked).Apply();
			this.CreateBinding(RadioButtonKalaniIptalEt).For((v) => v.Checked).To((VIOPTradeViewModel vm) => vm.KalaniIptalEtChecked).Apply();
			this.CreateBinding(RadioButtonKalaniPasifeYaz).For((v) => v.Checked).To((VIOPTradeViewModel vm) => vm.KalaniPasifeYazChecked).Apply();
			this.CreateBinding(RadioButtonLimitli).For((v) => v.Checked).To((VIOPTradeViewModel vm) => vm.LimitliChecked).Apply();
			this.CreateBinding(RadioButtonPiyasa).For((v) => v.Checked).To((VIOPTradeViewModel vm) => vm.PiyasaChecked).Apply();
			this.CreateBinding(RadioButtonTarihli).For((v) => v.Checked).To((VIOPTradeViewModel vm) => vm.TarihliChecked).Apply();

			this.CreateBinding(ViewChart).For((v) => v.Hidden).To((VIOPTradeViewModel vm) => vm.IsShowedSpecs).Apply();
			this.CreateBinding(ViewSpecs).For((v) => v.Hidden).To((VIOPTradeViewModel vm) => vm.IsShowedChart).Apply();
			this.CreateBinding(ButtonTabChart).For((v) => v.Checked).To((VIOPTradeViewModel vm) => vm.IsShowedChart).Apply();
			this.CreateBinding(ButtonTabSpecs).For((v) => v.Checked).To((VIOPTradeViewModel vm) => vm.IsShowedSpecs).Apply();

			this.CreateBinding(LabelAlisFiyat).For((v) => v.Text).To((VIOPTradeViewModel vm) => vm.VIOP.AlisFiyat).Apply();
			this.CreateBinding(LabelAlisDerinligi).For((v) => v.Text).To((VIOPTradeViewModel vm) => vm.VIOP.AlisDerinlik).Apply();
			this.CreateBinding(LabelSatisDerinligi).For((v) => v.Text).To((VIOPTradeViewModel vm) => vm.VIOP.SatisDerinlik).Apply();
			this.CreateBinding(LabelSatisFiyat).For((v) => v.Text).To((VIOPTradeViewModel vm) => vm.VIOP.SatisFiyat).Apply();
			this.CreateBinding(LabelTabanFiyat).For((v) => v.Text).To((VIOPTradeViewModel vm) => vm.VIOP.TabanFiyat).Apply();
			this.CreateBinding(LabelTavanFiyat).For((v) => v.Text).To((VIOPTradeViewModel vm) => vm.VIOP.TavanFiyat).Apply();

			this.CreateBinding(ButtonBuy).For((v) => v.TitleText).To((VIOPTradeViewModel vm) => vm.BuyMessage).Apply();
			this.CreateBinding(ButtonSell).For((v) => v.TitleText).To((VIOPTradeViewModel vm) => vm.SellMessage).Apply();
		}

		public override void ViewDidDisappear(bool animated)
		{
			base.ViewDidDisappear(animated);
			try
			{
				mTimer.Stop();
				mTimer.Dispose();
			}
			catch (Exception)
			{
			}
			mTimer = null;
		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);
			try
			{
				if (mTimer == null)
				{
					mTimer = new Timer();
					mTimer.Interval = mPeriod;
					mTimer.Elapsed += (sender, e) =>
					{
						if (!mIsRunning)
						{
							mIsRunning = true;
							(this.ViewModel as VIOPTradeViewModel).LoadStockWithoutProgressDialog();
							mIsRunning = false;
						}
					};
				}

				mTimer.Start();

			}
			catch (Exception)
			{
			}
		}

		public override void DidReceiveMemoryWarning()
		{
			base.DidReceiveMemoryWarning();
			// Release any cached data, images, etc that aren't in use.
		}

		// Invalidate the plot view when the orientation of the device changes
		public override void DidRotate(UIInterfaceOrientation fromInterfaceOrientation)
		{
			base.DidRotate(fromInterfaceOrientation);
			this.mPlotView.InvalidatePlot(false);
		}
	}
}


