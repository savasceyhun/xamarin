// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace MetaTrader.UI.IOS
{
    [Register ("VIOPsView")]
    partial class VIOPsView
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton ButtonBack { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        MetaTrader.UI.IOS.UIButtonDefault ButtonEmirler { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        MetaTrader.UI.IOS.UIButtonDefault ButtonHesapOzeti { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        MetaTrader.UI.IOS.UIButtonDefault ButtonPozisyonlar { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UISearchBar SearchBarVIOP { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableView TableVIOPs { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (ButtonBack != null) {
                ButtonBack.Dispose ();
                ButtonBack = null;
            }

            if (ButtonEmirler != null) {
                ButtonEmirler.Dispose ();
                ButtonEmirler = null;
            }

            if (ButtonHesapOzeti != null) {
                ButtonHesapOzeti.Dispose ();
                ButtonHesapOzeti = null;
            }

            if (ButtonPozisyonlar != null) {
                ButtonPozisyonlar.Dispose ();
                ButtonPozisyonlar = null;
            }

            if (SearchBarVIOP != null) {
                SearchBarVIOP.Dispose ();
                SearchBarVIOP = null;
            }

            if (TableVIOPs != null) {
                TableVIOPs.Dispose ();
                TableVIOPs = null;
            }
        }
    }
}