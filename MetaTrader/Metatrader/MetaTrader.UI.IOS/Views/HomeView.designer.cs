// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace MetaTrader.UI.IOS.Views
{
    [Register ("HomeView")]
    partial class HomeView
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        MetaTrader.UI.IOS.UIViewClickBlack BtnAccount { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        MetaTrader.UI.IOS.UIViewClickBlack BtnDerivatives { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        MetaTrader.UI.IOS.UIViewClickBlack BtnMoneyTranfers { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        MetaTrader.UI.IOS.UIViewClickBlack BtnStocks { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton ButtonBack { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (BtnAccount != null) {
                BtnAccount.Dispose ();
                BtnAccount = null;
            }

            if (BtnDerivatives != null) {
                BtnDerivatives.Dispose ();
                BtnDerivatives = null;
            }

            if (BtnMoneyTranfers != null) {
                BtnMoneyTranfers.Dispose ();
                BtnMoneyTranfers = null;
            }

            if (BtnStocks != null) {
                BtnStocks.Dispose ();
                BtnStocks = null;
            }

            if (ButtonBack != null) {
                ButtonBack.Dispose ();
                ButtonBack = null;
            }
        }
    }
}