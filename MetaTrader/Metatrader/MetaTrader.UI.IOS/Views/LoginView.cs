using MvvmCross.iOS.Views;
using System;

using UIKit;
using MvvmCross.Binding.BindingContext;
using MetaTrader.Core.ViewModels;
using CoreAnimation;
using CoreGraphics;

namespace MetaTrader.UI.IOS.Views
{
    public partial class LoginView : MvxViewBase
    {
        public LoginView() : base("LoginView", null)
        {
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

			this.CreateBinding(TfCode).For((v) => v.Text).To((LoginViewModel vm) => vm.Code).Apply();
			this.CreateBinding(TfLoginId).For((v) => v.Text).To((LoginViewModel vm) => vm.LoginId).Apply();
			this.CreateBinding(TfPassword).For((v) => v.Text).To((LoginViewModel vm) => vm.Password).Apply();
			this.CreateBinding(BtnLogin).To((LoginViewModel vm) => vm.LogOnCommand).Apply();
        }
    }
}