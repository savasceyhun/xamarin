﻿using System;

using UIKit;
using MvvmCross.iOS.Views;
using MvvmCross.Binding.iOS.Views;
using Foundation;
using MetaTrader.Core.Models;
using MvvmCross.Binding.BindingContext;
using System.Collections.Generic;
using MetaTrader.Core.ViewModels;
using ObjCRuntime;

namespace MetaTrader.UI.IOS
{
	public partial class StocksView : MvxViewBase
	{
		public StocksView () : base ("StocksView", null)
		{
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad();
			// Perform any additional setup after loading the view, typically from a nib.

			var source = new StockTableSource(TableStocks);
			this.AddBindings(new Dictionary<object, string>
				{
					{source, "ItemsSource StocksList"}
				});

			TableStocks.Source = source;
			TableStocks.ReloadData();

			this.CreateBinding(ButtonBack).For("Tap").To((StockConfirmOrderViewModel vm) => vm.BackCommand).Apply();
			this.CreateBinding(SearchBarStock).For((v) => v.Text).To((StocksViewModel vm) => vm.SearchText).Apply();

			SearchBarStock.EnablesReturnKeyAutomatically = false;
			SearchBarStock.SearchButtonClicked += (object sender, EventArgs e) => {
				SearchBarStock.ResignFirstResponder();
			};

			var protocol = Runtime.GetProtocol ("UITextInputTraits");
			foreach (var subview in SearchBarStock.Subviews) {
				foreach (var view in subview.Subviews) {					
					if (view.ConformsToProtocol (protocol)) {
						UITextField textField = (UITextField)view;
						textField.KeyboardAppearance = UIKeyboardAppearance.Alert;
						textField.ReturnKeyType = UIReturnKeyType.Done;
						textField.EnablesReturnKeyAutomatically = false;
					}
				}
			}

		}

		public override void DidReceiveMemoryWarning ()
		{
			base.DidReceiveMemoryWarning ();
		}


		public class StockTableSource : MvxSimpleTableViewSource
		{
			public StockTableSource(UITableView tableView)
				: base(tableView, "StockCell", "StockCell")
			{
			}


//			private static readonly NSString KittenCellIdentifier = new NSString("StockCell");
//
//			public TableSource(UITableView tableView)
//				: base(tableView)
//			{
//				tableView.RegisterNibForCellReuse(UINib.FromName("KittenCell", NSBundle.MainBundle), KittenCellIdentifier);
//			}
//
//			public override float GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
//			{
//				//return KittenCell.GetCellHeight();
//				return 30;
//			}
//
//			protected override UITableViewCell GetOrCreateCellFor(UITableView tableView, NSIndexPath indexPath,
//				object item)
//			{
//				NSString cellIdentifier;
//				if (item is Stock)
//				{
//					cellIdentifier = KittenCellIdentifier;
//				}
//				return (UITableViewCell) TableView.DequeueReusableCell(cellIdentifier, indexPath);
//			}

		}
	}
}


