// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace MetaTrader.UI.IOS
{
    [Register ("StockTradeView")]
    partial class StockTradeView
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton ButtonBack { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        MetaTrader.UI.IOS.UIButtonGreen ButtonBuy { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        MetaTrader.UI.IOS.UIButtonRed ButtonSell { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        MetaTrader.UI.IOS.UITabButton ButtonTabChart { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        MetaTrader.UI.IOS.UITabButton ButtonTabSpecs { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelBasePrice { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelCeilingPrice { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelDate { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelDescription { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelStockName { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelTickSize { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        MetaTrader.UI.IOS.RadioButton RadioButtonDengeleyici { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        MetaTrader.UI.IOS.RadioButton RadioButtonGunluk { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        MetaTrader.UI.IOS.RadioButton RadioButtonKIE { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        MetaTrader.UI.IOS.RadioButton RadioButtonLimit { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        MetaTrader.UI.IOS.RadioButton RadioButtonPiyasa { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        MetaTrader.UI.IOS.RadioButton RadioButtonPiyasadanLimite { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        MetaTrader.UI.IOS.UITextViewDefault TextViewAmount { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        MetaTrader.UI.IOS.UITextViewDefault TextViewPrice { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView UIViewTab { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView ViewChart { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        MetaTrader.UI.IOS.UIViewRounded ViewOrderMethod { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        MetaTrader.UI.IOS.UIViewRounded ViewOrderType { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView ViewSpecs { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (ButtonBack != null) {
                ButtonBack.Dispose ();
                ButtonBack = null;
            }

            if (ButtonBuy != null) {
                ButtonBuy.Dispose ();
                ButtonBuy = null;
            }

            if (ButtonSell != null) {
                ButtonSell.Dispose ();
                ButtonSell = null;
            }

            if (ButtonTabChart != null) {
                ButtonTabChart.Dispose ();
                ButtonTabChart = null;
            }

            if (ButtonTabSpecs != null) {
                ButtonTabSpecs.Dispose ();
                ButtonTabSpecs = null;
            }

            if (LabelBasePrice != null) {
                LabelBasePrice.Dispose ();
                LabelBasePrice = null;
            }

            if (LabelCeilingPrice != null) {
                LabelCeilingPrice.Dispose ();
                LabelCeilingPrice = null;
            }

            if (LabelDate != null) {
                LabelDate.Dispose ();
                LabelDate = null;
            }

            if (LabelDescription != null) {
                LabelDescription.Dispose ();
                LabelDescription = null;
            }

            if (LabelStockName != null) {
                LabelStockName.Dispose ();
                LabelStockName = null;
            }

            if (LabelTickSize != null) {
                LabelTickSize.Dispose ();
                LabelTickSize = null;
            }

            if (RadioButtonDengeleyici != null) {
                RadioButtonDengeleyici.Dispose ();
                RadioButtonDengeleyici = null;
            }

            if (RadioButtonGunluk != null) {
                RadioButtonGunluk.Dispose ();
                RadioButtonGunluk = null;
            }

            if (RadioButtonKIE != null) {
                RadioButtonKIE.Dispose ();
                RadioButtonKIE = null;
            }

            if (RadioButtonLimit != null) {
                RadioButtonLimit.Dispose ();
                RadioButtonLimit = null;
            }

            if (RadioButtonPiyasa != null) {
                RadioButtonPiyasa.Dispose ();
                RadioButtonPiyasa = null;
            }

            if (RadioButtonPiyasadanLimite != null) {
                RadioButtonPiyasadanLimite.Dispose ();
                RadioButtonPiyasadanLimite = null;
            }

            if (TextViewAmount != null) {
                TextViewAmount.Dispose ();
                TextViewAmount = null;
            }

            if (TextViewPrice != null) {
                TextViewPrice.Dispose ();
                TextViewPrice = null;
            }

            if (UIViewTab != null) {
                UIViewTab.Dispose ();
                UIViewTab = null;
            }

            if (ViewChart != null) {
                ViewChart.Dispose ();
                ViewChart = null;
            }

            if (ViewOrderMethod != null) {
                ViewOrderMethod.Dispose ();
                ViewOrderMethod = null;
            }

            if (ViewOrderType != null) {
                ViewOrderType.Dispose ();
                ViewOrderType = null;
            }

            if (ViewSpecs != null) {
                ViewSpecs.Dispose ();
                ViewSpecs = null;
            }
        }
    }
}