using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;
using CoreGraphics;

namespace MetaTrader.UI.IOS
{
	partial class TextTabBarItem : UITabBarItem
	{
		public TextTabBarItem() : base()
		{
			this.InitControl();
		}

		public TextTabBarItem(UITabBarSystemItem tabBarSystemItem, nint tag) : base(tabBarSystemItem, tag)
		{
			this.InitControl();
		}

		public TextTabBarItem(string title, UIImage image, UIImage selectedImage) : base(title, image, selectedImage)
		{
			this.InitControl();
		}

		public TextTabBarItem(string title, UIImage image, nint tag) : base(title, image, tag)
		{
			this.InitControl();
		}

		public TextTabBarItem(NSCoder nsCoder) : base(nsCoder)
		{
			this.InitControl();
		}

		public TextTabBarItem(NSObjectFlag nsObjectFlag) : base(nsObjectFlag)
		{
			this.InitControl();
		}

		public TextTabBarItem (IntPtr handle) : base (handle)
		{
			this.InitControl();
		}

		private void InitControl()
		{

		}	 
	}
}
